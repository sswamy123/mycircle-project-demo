package com.guild.sherlock.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import com.guild.mycircle.pages.MyApps;
import com.guild.mycircle.util.Util;
import com.tavant.base.WebPage;
import com.tavant.kwutils.KWVariables;
import com.tavant.kwutils.MyTestCaseExecuter;
import com.tavant.utils.TwfException;

import jxl.read.biff.BiffException;

public class Sherlock extends WebPage{
	ArrayList <String> actualDropdownList = new ArrayList <String>();

	public void verifySherlockBorrowerDetails(String values) throws BiffException, InvalidFormatException, IOException, TwfException{
		System.out.println("**************** verifySherlockBorrowerDetails ***************");
		String args [] = KWVariables.getVariables().get(values).split(",");
		String elementType = args [0];
		String repoElements[] = args[1].split(":");
		String actualElementValues[] = args[2].split(":");
		String expectedElementValue="";
		String actualElementValue="";
		if(!elementType.equalsIgnoreCase("input")){
			for(int i=0;i<repoElements.length;i++){
				expectedElementValue = getElementByUsing(repoElements[i]).getText().trim();
				actualElementValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(actualElementValues[i]);
				actualElementValue=actualElementValue.contains("N/A")?"0":actualElementValue;
				expectedElementValue=expectedElementValue.contains("TEST_")?expectedElementValue.replace("TEST_","").trim():expectedElementValue;
				expectedElementValue = expectedElementValue.substring(expectedElementValue.indexOf(":")+1, expectedElementValue.length()).trim();
			
				// Its temporary fix for DTI, since currently we have different values in sherlock and MyApps
				// Ex: MyApp Ratio value : 4.070%/4.080%  and Sherlock DTI value : 14.467 for the App Id : 31469
				// Once the ratio / DTI mismatch issue is fixed the we have to remove the below condition.
				if(repoElements[i].length()>4 && repoElements[i].contains("ShL_DTI_value")){
					expectedElementValue=expectedElementValue.substring(0, 5);
					actualElementValue=actualElementValue.substring(0, 5);
				}
				
				System.out.println("expectedElementValue : actualElementValue :: "+expectedElementValue +" : "+actualElementValue);
				if(!expectedElementValue.equalsIgnoreCase(actualElementValue)){
					Util.addExceptionToReport("Values mismatch", actualElementValue, expectedElementValue);
				}
			}
		}
		else if(elementType.equalsIgnoreCase("input")){
			for(int j=0;j<repoElements.length;j++){
				expectedElementValue = getElementByUsing(repoElements[j]).getAttribute("value").trim();
				actualElementValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(actualElementValues[j]);
				actualElementValue=actualElementValue.contains("N/A")?"0":actualElementValue;
				
				// Its temporary fix for DTI, since currently we have different values in sherlock and MyApps
				// Ex: MyApp Ratio value : 4.070%/4.080%  and Sherlock DTI value : 14.467 for the App Id : 31469
				// Once the ratio / DTI mismatch issue is fixed the we have to remove the below condition.
				if(repoElements[j].length()>4 && repoElements[j].contains("ShL_DTI_value")){
					expectedElementValue=expectedElementValue.substring(0, 5);
					actualElementValue=actualElementValue.substring(0, 5);
				}
				
				System.out.println("expectedElementValue : actualElementValue ==> : "+expectedElementValue+" : "+actualElementValue);
				if(!expectedElementValue.equalsIgnoreCase(actualElementValues[j])){
					System.out.println("actualElementValues"+actualElementValues+"expectedElementValue"+expectedElementValue);
					//Util.addExceptionToReport("Values mismatch", actualElementValues[j], expectedElementValue);
				}
			}
		}
	}

	// This method is temporary fix to search applicatio id from sherlock, since
	// currently sharelock button is not working in MyApp page,
	// Once the sharelock button functionlity is fixed in MyApps page then we
	// need to remove searchSherlockWithAppId method.
	public void searchSherlockWithAppId(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		System.out.println("****************** searchSherlockWithAppId **************** ");
		String args[] = KWVariables.getVariables().get(values).split(":");
		System.out.println("applicationId :: " + MyApps.applicationId);
		getElementByUsing(args[0]).sendKeys(MyApps.applicationId);
		waitForElement(getElementByUsing(args[1]), 120);
		getElementByUsing(args[2]).click();
		Thread.sleep(5000);
		System.out.println("ShL_App_Search_btn : " + getElementByUsing(args[1]).getText());
		getElementByUsing(args[0]).sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		System.out.println("******************* Sherlock search with AppId is done !!!!!!!!!!!! ************************");
	}
	
	public void verifySherlockPropertyDropdownSelectedOptions(String values) throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException{
		System.out.println("************** verifySherlockPropertyDropdownSelectedOptions *************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		ArrayList <String> list = MyApps.selectedDropdownOptions;
		List<String> expectedDropdownList = list.subList(((list.size())-4), (list.size()));
		String loanProgramType_dd = args[0];
		String selectedOption = "";

		for (int i=0;i<args.length;i++){
			waitForElement(getElementByUsing(args[i]), 60);
			selectedOption = new Select(getElementByUsing(args[i])).getFirstSelectedOption().getText();
			Thread.sleep(2000);
			if(args[i].equalsIgnoreCase(loanProgramType_dd)){
				selectedOption=selectedOption.substring(0, selectedOption.lastIndexOf("-")-1);
				System.out.println("the loan prgm selected option :****: "+selectedOption);
			}
			actualDropdownList.add(selectedOption);
		}
		
		System.out.println("expectedDropdownList : actualDropdownList **********>> "+expectedDropdownList+" : "+actualDropdownList);

		for (int j=0; j<expectedDropdownList.size();j++){
			if(!expectedDropdownList.get(j).equalsIgnoreCase(actualDropdownList.get(j))){
				Util.addExceptionToReport("Dropdown selected values mismath ", actualDropdownList.get(j),expectedDropdownList.get(j) );
			}
		}
		
		System.out.println("expectedDropdownList : actualDropdownList ==========> "+actualDropdownList +" : "+expectedDropdownList);
	}
	
	public void verifySherlockPropertyResetFormSection(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("************* verifySherlockPropertyResetFormSection ***************");
		String args[] = KWVariables.getVariables().get(values).split(",")[0].split(":");
		String args1[] = KWVariables.getVariables().get(values).split(",")[1].split(":");
		ArrayList<String> expectedDropdownList = new ArrayList<String>();
		ArrayList<String> actualDropdownList = new ArrayList<String>();

		String expectedElementValue = "";
		String actualElementValue = "";
		for (int i = 0; i < args.length; i++) {
			expectedElementValue = getElementByUsing(args[i]).getText().trim();
			expectedElementValue = expectedElementValue.substring(expectedElementValue.indexOf(":")+2,expectedElementValue.length());
			expectedDropdownList.add(expectedElementValue);
		}
		for (int j = 0; j < args1.length; j++) {
			actualElementValue = new Select(getElementByUsing(args1[j])).getFirstSelectedOption().getText();
			actualDropdownList.add(actualElementValue);
		}
		if (!expectedDropdownList.equals(actualDropdownList)) {
			Util.addExceptionToReport("Dropdown selected values mismath ", actualDropdownList+"",
					actualDropdownList+"");
		}
	}
	
	@Override
	public void checkPage() {
		// TODO Auto-generated method stub
	}
}
