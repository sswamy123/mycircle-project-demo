package com.guild.mycircle.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.tavant.base.DriverFactory;
import com.tavant.kwutils.KWVariables;
import com.tavant.utils.TwfException;
import au.com.bytecode.opencsv.CSVWriter;
import jxl.read.biff.BiffException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class Util {

	/**
	 * Add Failure Reason to Report
	 * 
	 * @param description
	 * @param actualValue
	 * @param expectedValue
	 * @return
	 * @throws TwfException
	 */
	public static void addExceptionToReport(String description, String actualValue, String expectedValue)
			throws TwfException {
		throw new TwfException(description + " :<font color=\"solid orange\">  Actual :[" + actualValue
				+ "]</font><font color=\"EE7600\"> Expected :[" + expectedValue + "]</font><br> <b>Step Details:</b> "
				+ "<br>");
	}

	/**
	 * Gets the window handles and switches between the windows
	 * 
	 * @throws TwfException
	 * 
	 * @throws InterruptedException
	 */
	public static void switchToWindow(WebDriver driver) throws TwfException, InterruptedException {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			System.out.println("winHandle ==> " + winHandle);
		}
	}

	public static boolean isElementPresentOnPage(WebDriver driver, By value) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		if (driver.findElements(value).size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean ischeckBoxSelected(WebDriver driver, By value) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		if (driver.findElements(value).size() > 0) {
			if (driver.findElement(value).isSelected()) {
				return true;
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	/**
	 * Verifies the option box status, its its not selected then it selects,
	 * 
	 * @param expectedCheckboxStatus
	 * @param weCheckbox
	 * @throws InterruptedException
	 */
	public static void selectCheckBoxOption(WebElement weCheckbox) throws InterruptedException {
		if (!weCheckbox.isSelected()) {
			weCheckbox.click();
			Thread.sleep(1000);
		} else {
			System.out.println("Option already selected");
		}
	}

	/**
	 * Click Alert OK button and verifies the alert text
	 * @author muni.reddy
	 * @param value
	 * @throws TwfException
	 * @throws BiffExceptiona
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public static void getAlertpopup(String val, String borrName)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		WebDriver driver = DriverFactory.getDriver();
		String actualAlertText;
		String paramsFromActionSteps[] = val.split(",");
		String[] arr = KWVariables.getVariables().get(paramsFromActionSteps[0]).split(":");
		String expectedAlertText = arr[1].replace(arr[2], borrName).trim();
		try {
			Thread.sleep(1000);
			driver.findElement(By.id(arr[0])).click();
			Thread.sleep(1000);
			Alert alert = driver.switchTo().alert();
			actualAlertText = alert.getText().trim();
			alert.accept();
			if (!actualAlertText.equalsIgnoreCase(expectedAlertText)) {
				Util.addExceptionToReport("Alert text mismatch", actualAlertText, expectedAlertText);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	
	/**
	 * Get date based on the format and the numbers of days requested by adding / subtracting
	 * @author muni.reddy
	 * @param strDateFormat
	 * @param days
	 * @return
	 */
	public static String getDateToSearch(String strDateFormat, String expectedType, int days, String actualTypeToAdd ) {
		SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		GregorianCalendar cal = new GregorianCalendar();
		Date date = new Date();
		cal.setTime(date);
		if (expectedType.equalsIgnoreCase("date")) {
			cal.add(Calendar.DATE, days);
		} else if (expectedType.equalsIgnoreCase("months")) {
			cal.add(Calendar.MONTH, days);
		} else if (expectedType.equalsIgnoreCase("years")) {
			cal.add(Calendar.YEAR, days);
		}

		System.out.println("sdf.format(cal.getTime()) :: "+sdf.format(cal.getTime()));
		return sdf.format(cal.getTime());
	}

	/**
	 * Create CSV file
	 * @param values
	 */
	public static void createCSVFile(String values) {
		String resultFileDateTime = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss").format(new Date())
				.replaceAll("[\\/\\-\\:]", "_");
		String resultFolderDate = new SimpleDateFormat("MM_dd_yyyy").format(new Date()).replaceAll("[\\/\\-\\:]", "_");
		File currentResDir = new File(System.getProperty("user.dir") + File.separator + "csvfiles" + File.separator
				+ "FailedLoansOn_" + resultFolderDate);

		currentResDir.mkdirs();
		String failedLoansCSVFilePath = currentResDir.getAbsolutePath() + File.separator + "MyAppApplicationNumbers_"
				+ "Loans_Report_" + resultFileDateTime + ".csv";
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(failedLoansCSVFilePath));
			List<String[]> csvData = new ArrayList<String[]>();
			String[] profile = values.split("\n");
			String reportColumns = "SLNo,LOAN NUMBER";
			String[] header = reportColumns.split(",");
			csvData.add(header);
			for (int i = 0; i < profile.length; i++) {
				String[] mr = profile[i].split(",");
				csvData.add(mr);
			}

			writer.writeAll(csvData);
			writer.close();
			System.out.println("CSV file created succesfully.");

		} catch (Exception e) {
			System.out.println("Exception :" + e.getMessage());
		}
	}
	
	/**
	 * Delete all cookies
	 * @param driver
	 * @param values
	 */
	public static void deleteAllCookies(WebDriver driver)
	{
		System.out.println("*************** deleteAllCookies ************");
		driver.manage().deleteAllCookies();
		System.out.println("Deleteting of cookies is done !!!!!!!!!!!!");
	}

}