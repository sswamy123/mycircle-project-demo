package com.guild.mycircle.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.guild.mycircle.util.Util;
import com.tavant.base.DriverFactory;
import com.tavant.base.WebPage;
import com.tavant.kwutils.KWVariables;
import com.tavant.kwutils.MyTestCaseExecuter;
import com.tavant.utils.TwfException;

import jxl.read.biff.BiffException;

public class WebApps extends WebPage {

	public static String appid;
	int elementDisplayTimeOut = 15;
	WebDriver driver;

	/**
	 * Verifies the borrower name
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyBorrowername(String values) throws Exception, IOException, TwfException {
		String args[] = values.split(",");
		String appheader = getElementByUsing(args[0]).getText();

		String borrfullname = MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[1]);
		if (!borrfullname.equalsIgnoreCase("No data available in table")) {

			if (!borrfullname.equals("Not Provided")) {
				String borr_fname = borrfullname.split(",")[1];
				String borr_lname = borrfullname.split(",")[0];
				if (!appheader.contains(borr_fname) && !appheader.contains(borr_lname)) {
					Util.addExceptionToReport("Mismatch in Borrower Name", appheader, borr_fname + " " + borr_lname);
				}
			}
		} else {

			Util.addExceptionToReport("Data is not available in dashboard, please change test data ",
					"There should have data", "here should have some data");
		}
	}

	
	/**
	 * Verifies the test data presence on dash board
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyWebAppsData(String values) throws Exception, IOException, TwfException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String borrowername = getElementByUsing(args[0]).getText();
		System.out.println("borrowername :: " + borrowername);
		if (!borrowername.equalsIgnoreCase(args[1]) && borrowername.length() > 1) {
			System.out.println("Have data on dashboaed!!!!!!!");
		} else {
			Util.addExceptionToReport("Data is not available in dashboard, please change test data ",borrowername, "There should have data");
		}
	}
	
	/**
	 * Verifies the sorting of column data in ascending and descending orders
	 * 
	 * @author muni.reddy
	 * @param columnnnumber
	 *            to be sorted
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws BiffException
	 * @throws IOException
	 */
	public void verifySortingOfItemsDisplayed1(String valuesTps)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		WebDriver driver = DriverFactory.getDriver();
		String values[] = KWVariables.getVariables().get(valuesTps).split(":");
		String tableHeaderPreXpath = values[0];
		String tableHeaderPostXpath = values[1];
		String tableBodyPreXpath = values[2];
		String tableBodyPostXpath = values[3];
		String pageNumXpath = values[5];
		String columnNo = values[6];
		List<String> unsortedList = new ArrayList<String>();
		List<String> afterSortingcolumnFirstList = new ArrayList<String>();
		List<String> afterSortingcolumnSecondList = new ArrayList<String>();
		int noofPages = driver.findElements(By.xpath("//*[@id=\"DataTables_Table_1_paginate\"]/span/a")).size();
		String nextpage = "";
		for (int i = 0; i < noofPages; i++) {
			unsortedList.addAll(getListOfItemsDisplaying(tableBodyPreXpath, tableBodyPostXpath, columnNo));
			if ((i + 1) < noofPages) {
				nextpage = pageNumXpath + "[" + (i + 1) + "]";
				driver.findElement(By.xpath(nextpage)).click();
				Thread.sleep(3000);
			}
		}
		List<String> blanklist = new ArrayList<>();
		List<String> ascendingOrder = new ArrayList<>();
		List<String> descscendingOrder = new ArrayList<>();
		List<String> loanamount = new ArrayList<>();
		// sorting of Currency Columns
		if (columnNo.equals("3")) {
			for (int i = 0; i < unsortedList.size(); i++) {
				loanamount.add(unsortedList.get(i).trim());
			}
			Collections.sort(loanamount);
			unsortedList.clear();
			for (int i = 0; i < loanamount.size(); i++) {
				unsortedList.add(loanamount.get(i).toString());
			}
			if (unsortedList.size() >= 100) {
				ascendingOrder = unsortedList.subList(0, 100);
			} else {
				ascendingOrder = unsortedList;
			}
		} else {
			for (int i = 0; i < unsortedList.size(); i++) {
				if (unsortedList.get(i).equals("N/A")) {
					blanklist.add(unsortedList.get(i));
				}
			}
			if (blanklist.size() > 0) {
				unsortedList.removeAll(blanklist);
			}
			if (columnNo.equals("1"))
				unsortedList.sort(new MyComp());
			else
				Collections.sort(unsortedList);

			if (blanklist.size() >= 100) {
				ascendingOrder.addAll(blanklist);
				ascendingOrder.addAll(unsortedList);
				ascendingOrder = ascendingOrder.subList(0, 100);
			} else {
				ascendingOrder.addAll(blanklist);
				ascendingOrder.addAll(unsortedList);
			}
		}
		clickOnColumnHeader(tableHeaderPreXpath, tableHeaderPostXpath, columnNo);
		Thread.sleep(5000);
		afterSortingcolumnFirstList = getListOfItemsDisplaying(tableBodyPreXpath, tableBodyPostXpath, columnNo);
		Thread.sleep(2000);
		if (!ascendingOrder.equals(afterSortingcolumnFirstList)) {
			Util.addExceptionToReport("Sorting of ascending order: ", afterSortingcolumnFirstList + "",
					ascendingOrder + "");
		}
		Collections.reverse(unsortedList);
		if (blanklist.size() > 0) {
			unsortedList.addAll(blanklist);
		}
		if (unsortedList.size() >= 100) {
			descscendingOrder = unsortedList.subList(0, 100);
		} else {
			descscendingOrder = unsortedList;
		}
		clickOnColumnHeader(tableHeaderPreXpath, tableHeaderPostXpath, columnNo);
		Thread.sleep(5000);
		afterSortingcolumnSecondList = getListOfItemsDisplaying(tableBodyPreXpath, tableBodyPostXpath, columnNo);
		if (!descscendingOrder.equals(afterSortingcolumnSecondList)) {
			Util.addExceptionToReport("Sorting of Descending order: ", afterSortingcolumnSecondList + "",
					descscendingOrder + "");
		}
	}

	/**
	 * Displays all the items displayed in the dash board
	 * 
	 * @param columnn
	 *            number to be sorted
	 * 
	 * @throws TwfException
	 * 
	 * @throws InterruptedException
	 */
	public List<String> getListOfItemsDisplaying(String tableHeaderPreXpath, String tableBodyPostXpath, String columnNo)
			throws TwfException, InterruptedException {
		WebDriver driver = DriverFactory.getDriver();
		List<WebElement> elemnets;
		int itemIndex = 1;
		String Id;
		List<String> unsortedList = new ArrayList<String>();
		elemnets = driver.findElements(By.xpath(tableHeaderPreXpath + tableBodyPostXpath + "[" + columnNo + "]"));
		int numberOfRowsDisplayed = elemnets.size();
		for (int i = 1; i <= (numberOfRowsDisplayed); i++) {
			WebElement we = driver.findElement(
					By.xpath(tableHeaderPreXpath + "[" + itemIndex + "]" + tableBodyPostXpath + "[" + columnNo + "]"));
			if (columnNo.equals("1") || columnNo.equals("4")) {
				Id = we.getText().split(",")[0];
			} else if (columnNo.equals("10")) {
				Id = we.getText().substring(we.getText().length() - 4, we.getText().length() - 1);
			} else {
				Id = we.getText();
			}
			unsortedList.add(Id);
			if (itemIndex < numberOfRowsDisplayed) {
				itemIndex++;
			}
		}
		return unsortedList;
	}

	/**
	 * Clicks on the column number to sort
	 * 
	 * @author muni.reddy
	 * @param columnn
	 *            number to be sorted
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void clickOnColumnHeader(String tableHeaderPreXpath, String tableHeaderPostXpath, String columnNumber)
			throws TwfException, InterruptedException {
		WebDriver driver = DriverFactory.getDriver();
		WebElement columnHeader = driver
				.findElement(By.xpath(tableHeaderPreXpath + tableHeaderPostXpath + "[" + columnNumber + "]"));
		columnHeader.click();
		Thread.sleep(6000);
	}

	/**
	 * Verifies the web apps delete functionality
	 * 
	 * @param values
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	public void verifyWebAppdelete(String values) throws InvalidFormatException, IOException, Exception {
		System.out.println("*********************** verifyWebAppdelete **********************");
		WebDriver driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		String borrowername = getElementByUsing(args[0]).getText();
		System.out.println("borrowername :: " + borrowername);
		if (!borrowername.equalsIgnoreCase("No data available in table") && borrowername.length() > 1) {
			borrowername = borrowername.split(",")[0];
			appid = driver.findElement(By.xpath(args[2])).getAttribute("id");
			appid = appid.split("-")[1];
			getElementByUsing(args[1]).click();
			Alert deleteAlert = driver.switchTo().alert();
			String alertText = deleteAlert.getText();
			if (!alertText.contains(appid)) {
				Util.addExceptionToReport("Mismatch in App id of deleting record", alertText, appid);
			}
			deleteAlert.accept();
			Thread.sleep(2000);
			if (!getElementByUsing(args[4]).getText().trim()
					.equalsIgnoreCase("The application was successfully deleted.")) {
				Util.addExceptionToReport("Error message is not correct", getElementByUsing(args[4]).getText().trim(),
						"The application was successfully deleted.");
			}
			driver.navigate().refresh();
			Thread.sleep(3000);
			getElementByUsing(args[3]).sendKeys(borrowername);
			Thread.sleep(3000);
			if ((driver.findElement(By.xpath(args[2])).getAttribute("id").contains(appid))) {
				Util.addExceptionToReport("Deleted App is still in the Pipeline",
						(driver.findElement(By.xpath(args[2]))).getText(), "Should not be Present");
			}
		} else {

			Util.addExceptionToReport("Data is not available in dashboard, please change test data ",
					borrowername, "These should be some data");
		}
	}

	class MyComp implements Comparator<String> {
		@Override
		public int compare(String o1, String o2) {
			return (o1.compareToIgnoreCase(o2));
		}
	}

	/**
	 * Submits the web apps
	 * 
	 * @param values
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws Exception
	 */
	public void submitWebApp(String values) throws InvalidFormatException, IOException, Exception {
		System.out.println("*************** submitWebApp *****************");
		WebDriver driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		String borrowername = getElementByUsing(args[0]).getText();
		if (!borrowername.equalsIgnoreCase("No data available in table") && borrowername.length() > 1) {

			if (args[2].equalsIgnoreCase("pipeline")) {
				appid = getElementByUsing(args[1]).getAttribute("value");
			}
			if (args[1].equalsIgnoreCase("1003")) {
				appid = getElementByUsing(args[1]).getAttribute("data-app-id");
			}
			getElementByUsing(args[1]).click();
			// Thread.sleep(2000);
			Alert submitAlert = driver.switchTo().alert();
			Thread.sleep(2000);
			String submitText = submitAlert.getText();

			if (!submitText.contains(appid) && (args[2].equalsIgnoreCase("pipeline"))) {
				Util.addExceptionToReport("Mismatch in App id of Submitting record", submitText, appid);
			}
			Thread.sleep(3000);
			submitAlert.accept();
		}
	else
	{
		Util.addExceptionToReport("Data is not available in dashboard, please change test data ",
				"There should have data", "No data available in table");
	}Thread.sleep(2000);
	}
	
	/**
	 * Verifies the app id
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyAppID(String values) throws Exception, IOException, TwfException {
		if (!String.valueOf(values).equalsIgnoreCase(appid)) {
			Util.addExceptionToReport("Mismatch in App id of Submitting record", values, appid);
		}
	}

	/**
	 * Verifies the apps in pipeline
	 * 
	 * @param values
	 * @throws Exception
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void verifyAppInPipeline(String values) throws Exception, InvalidFormatException, IOException {
		WebDriver driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		String borrfullname = MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[0]);
		// String borr_fname = borrfullname.split(",")[1];
		String borr_lname = borrfullname.split(",")[0];
		getElementByUsing(args[2]).sendKeys(borr_lname);
		Thread.sleep(3000);
		if ((driver.findElement(By.xpath(args[1])).getAttribute("id").contains(appid))) {
			Util.addExceptionToReport("Submitted App is still in the Pipeline",
					(driver.findElement(By.xpath(args[1]))).getText(), "Should not be Present");
		}
	}
	
	public boolean waitTillElementToDisappear(String element, int waitTimeInSeconds)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		boolean displayed = true;
		int maxtime = 0;
		boolean isVisible = true;
		while (displayed) {
			isVisible = getElementByUsing(element).isDisplayed();
			if (!isVisible) {
				break;
			} else if (maxtime < waitTimeInSeconds) {
				maxtime = maxtime + 1;
				Thread.sleep(1000);
			}
		}
		return isVisible;
	}
	
	public void performTabOut(String values) throws Exception, IOException, TwfException {
		System.out.println("****************** performTabOut ********************");
		System.out.println("values" + values);
		driver = DriverFactory.getDriver();
		Thread.sleep(2000);
		getElementByUsing(values).sendKeys(Keys.TAB);
		Thread.sleep(1000);
	}
	
	public void verifySearchFunctionalityForWebApps(String values) throws Exception {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String repoEle[] = args[0].split(",");
		String val[] = args[1].split(",");
		String searchString;

		// Search for test based on date
		if (!val[0].equalsIgnoreCase("date")) {
			searchString = val[0];
		} else {
			searchString = Util.getDateToSearch(val[2], val[3], Integer.parseInt(val[4]), val[5]);
		}

		// Wait for Clear Filters button
		waitForElement(getElementByUsing(repoEle[0]), elementDisplayTimeOut);
		waitTillElementToDisappear(repoEle[1], elementDisplayTimeOut);

		// Type the value in search text box
		getElementByUsing(repoEle[2]).sendKeys(searchString);
		performTabOut(repoEle[2]);

		// Wait for Clear Filters button
		waitForElement(getElementByUsing(repoEle[0]), 20);
		getElementByUsing(repoEle[2]).click();

		waitTillElementToDisappear(repoEle[1], elementDisplayTimeOut);
		// Get all the result values from the table

		List<WebElement> myAppsListColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(val[1]));

		String actualValue = "";
		if (myAppsListColumnDataDisplayed.size() > 0) {
			for (int i = 0; i < myAppsListColumnDataDisplayed.size(); i++) {
				actualValue = myAppsListColumnDataDisplayed.get(i).getText().trim();
				if (actualValue.contains("$")) {
					actualValue = actualValue.replaceAll("$", "").replaceAll(",", "");

				}
				// Added for Webapps Pipeline
				if (actualValue.contains("-")) {
					actualValue = actualValue.replaceAll("-", "").replaceAll(",", "");
				}
				if (!actualValue.contains(searchString)) {
					Util.addExceptionToReport("Filtering of loans failed", actualValue, searchString);
				}
			}

		} else if (val[1] != null && val[1].equalsIgnoreCase("no records")) {
			actualValue = getElementByUsing(repoEle[3]).getText().trim();
			if (!actualValue.equalsIgnoreCase(val[2])) {
				Util.addExceptionToReport("Filtering of loans failed", actualValue, val[2]);
			}
		}
	}

	@Override
	public void checkPage() {
		// TODO Auto-generated method stub
	}
}
