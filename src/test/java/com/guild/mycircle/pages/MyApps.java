package com.guild.mycircle.pages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.text.NumberFormat;
import java.text.ParseException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.SystemPropertyUtils;

import com.guild.mycircle.util.*;
import com.tavant.base.DriverFactory;
import com.tavant.base.WebPage;
import com.tavant.kwutils.KWVariables;
import com.tavant.kwutils.MyTestCaseExecuter;
import com.tavant.utils.TwfException;
import jxl.read.biff.BiffException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class MyApps extends WebPage {
	WebDriver driver;
	static String borrowerName;
	static String quickAppId;
	static boolean guildToGuildRft = false;
	public static String applicationId;
	static String respaDate;
	static ArrayList<String> applicationValues;
	public static ArrayList<String> selectedDropdownOptions = new ArrayList<String>();
	static String selectedOption;
	int elementDisplayTimeOut = 15;
	static String selectedOption_2;

	/**
	 * Verifies the login functionality with valid user credentials
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyLogin(String values)
            throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
        System.out.println("************* verifyLogin(String values) ************* ");
        driver = DriverFactory.getDriver();
        String params = KWVariables.getVariables().get(values);
        String args[] = params.split(":");
        getElementByUsing(args[0]).clear();
        getElementByUsing(args[0]).sendKeys(args[5]);
        getElementByUsing(args[1]).clear();
        getElementByUsing(args[1]).sendKeys(args[6]);
        getElementByUsing(args[2]).click();

 

        // Wait for Clear Filters button
        waitForElement(getElementByUsing(args[3]), elementDisplayTimeOut);
        String welcomeUserText = getElementByUsing(args[3]).getText();
        String homePageMyAccLink = getElementByUsing(args[4]).getText();
        if (!getElementByUsing(args[3]).isDisplayed()) {
            Util.addExceptionToReport("Login Failed", welcomeUserText, args[7]);
        }
    }

	/**
	 * Verifies login functionality with invalid user credentials
	 * 
	 * @author muni.reddy
	 * @param dataPoolArgs
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyInvalidLoginCredentialsErrors(Map<String, String> dataPoolArgs)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		// Get data from Data pool column "RepoElements"
		String elements[] = dataPoolArgs.get("RepoElements").split(",");

		// Get data from Data pool column "DataColumns"
		String localDataColumns[] = dataPoolArgs.get("DataColumns").split(",");
		for (int i = 0; i < localDataColumns.length; i++) {
			String values[] = dataPoolArgs.get(localDataColumns[i]).split("::");
			getElementByUsing(elements[0]).clear();
			getElementByUsing(elements[0]).sendKeys(values[1].trim());
			getElementByUsing(elements[1]).clear();
			getElementByUsing(elements[1]).sendKeys(values[2].trim());
			getElementByUsing(elements[2]).click();
			String actualErrorText = getElementByUsing(elements[3]).getText();

			if (!values[0].equalsIgnoreCase(actualErrorText)) {
				Util.addExceptionToReport("Login error text mismatch", actualErrorText, values[0]);
			}
		}
	}

	/**
	 * Verifies the more filters functionality
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyMoreFiltersFunctionality(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		String params = KWVariables.getVariables().get(values);
		String args[] = params.split(":");
		getElementByUsing(args[0]).clear();
		getElementByUsing(args[0]).sendKeys(args[6]);
		Thread.sleep(3000);
		getElementByUsing(args[5]).click();
		Thread.sleep(3000);

		if (getSize(args[1]) > 0) {
			getElementByUsing(args[2]).click();
			waitForElement(getElementByUsing(args[3]), elementDisplayTimeOut);
			String actualValue = getElementByUsing(args[4]).getAttribute(args[7]);
			if (!actualValue.equals(args[6])) {
				Util.addExceptionToReport("Filter values mismatch", actualValue, args[6]);
			}
		}
	}

	/**
	 * Stores dynamic borrower name for future usage
	 * 
	 * @author muni.reddy
	 * @param value
	 * @throws BiffException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	public void storeBorrowerName(String value)
			throws BiffException, TwfException, InterruptedException, IOException, InvalidFormatException {
		String paramsFromActionSteps[] = value.split(",");
		String borrFName = MyTestCaseExecuter.stepobject.getKwValueVariables().get(paramsFromActionSteps[1]);
		String borrLName = MyTestCaseExecuter.stepobject.getKwValueVariables().get(paramsFromActionSteps[2]);
		borrowerName = borrFName + " " + borrLName;
	}

	/**
	 * Stores mandatory drop-down selected options to verify in Sherlock page
	 * 
	 * @param values
	 * @return
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public ArrayList<String> storeDropdownSelectedOptions(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {

		String args[] = KWVariables.getVariables().get(values).split(":");
		String selectedOption = "";

		for (int i = 0; i < args.length; i++) {
			selectedOption = new Select(getElementByUsing(args[i])).getFirstSelectedOption().getText();
			if (args[i].equalsIgnoreCase(args[0])) {
				selectedOption = selectedOption.substring(0, selectedOption.lastIndexOf("(") - 1);
			}
			selectedDropdownOptions.add(selectedOption);
		}
		return selectedDropdownOptions;
	}

	/**
	 * Gets the window handles and switches between the windows
	 * 
	 * @author muni.reddy
	 * @throws TwfException
	 *             * @throws InterruptedException
	 */
	public void switchToWindow(String values) throws TwfException, InterruptedException {
		WebDriver driver = DriverFactory.getDriver();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	}

	/**
	 * Handle alerts
	 * 
	 * @author muni.reddy
	 * @param val
	 * @throws BiffException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws InvalidFormatException
	 */
	public void handleAlerts(String val)
			throws BiffException, TwfException, InterruptedException, IOException, InvalidFormatException {
		Util.getAlertpopup(val, borrowerName);
	}

	/**
	 * Search loans based on search filters
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifySearchFunctionality(String values) throws Exception {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String repoEle[] = args[0].split(",");
		String val[] = args[1].split(",");
		String searchString;

		// Search for test based on date
		if (!val[0].equalsIgnoreCase("date")) {
			searchString = val[0];
		} else {
			searchString = Util.getDateToSearch(val[2], val[3], Integer.parseInt(val[4]), val[5]);
		}

		// Wait for Clear Filters button
		waitForElement(getElementByUsing(repoEle[0]), elementDisplayTimeOut);
		waitTillElementToDisappear(repoEle[1], elementDisplayTimeOut);

		// Type the value in search text box
		getElementByUsing(repoEle[2]).sendKeys(searchString);
		performTabOut(repoEle[2]);

		// Wait for Clear Filters button
		waitForElement(getElementByUsing(repoEle[0]), 20);
		getElementByUsing(repoEle[2]).click();

		waitTillElementToDisappear(repoEle[1], elementDisplayTimeOut);
		// Get all the result values from the table

		List<WebElement> myAppsListColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(val[1]));

		String actualValue = "";
		if (myAppsListColumnDataDisplayed.size() > 0) {
			for (int i = 0; i < myAppsListColumnDataDisplayed.size(); i++) {
				actualValue = myAppsListColumnDataDisplayed.get(i).getText().trim();
				if (actualValue.contains("$")) {
					actualValue = actualValue.replaceAll("$", "").replaceAll(",", "");

				}
				// Added for Webapps Pipeline
				if (actualValue.contains("-")) {
					actualValue = actualValue.replaceAll("-", "").replaceAll(",", "");
				}
				if (!actualValue.contains(searchString)) {
					Util.addExceptionToReport("Filtering of loans failed", actualValue, searchString);
				}
			}

		} else if (val[1] != null && val[1].equalsIgnoreCase("no records")) {
			actualValue = getElementByUsing(repoEle[3]).getText().trim();
			if (!actualValue.equalsIgnoreCase(val[2])) {
				Util.addExceptionToReport("Filtering of loans failed", actualValue, val[2]);
			}
		}
	}

	/**
	 * Verifies the filters functionality in WebApps page
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void verifyFiltersFunctionality(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		Select filterOptions = new Select(getElementByUsing(args[1]));
		int noOfOptionsToCheck = Integer.parseInt(args[2]);
		int actualNumberOgDDOption = filterOptions.getOptions().size();

		if (actualNumberOgDDOption <= noOfOptionsToCheck) {
			noOfOptionsToCheck = actualNumberOgDDOption;
		}
		for (int i = 1; i < noOfOptionsToCheck; i++) {
			// Select option based on index
			filterOptions.selectByIndex(i);

			// Wait for Clear Filters button
			waitForElement(getElementByUsing(args[3]), elementDisplayTimeOut);

			// Added this sleep time, since some times app is very fast
			Thread.sleep(3000);
			waitTillElementToDisappear(args[0], elementDisplayTimeOut);

			// Get all the result values from the table
			List<WebElement> myAppsListColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(args[4]));

			String actualValue = filterOptions.getFirstSelectedOption().getText().trim().toUpperCase();

			if (getElementByUsing(args[1]).getAttribute(args[5]).equalsIgnoreCase(args[6])
					|| getElementByUsing(args[1]).getAttribute(args[5]).equalsIgnoreCase(args[7])) {
				actualValue = actualValue.substring(0, 1);
			} else {
				actualValue = actualValue.substring(0, actualValue.indexOf("(") - 1);
			}

			if (myAppsListColumnDataDisplayed.size() > 0) {
				for (int j = 0; j < myAppsListColumnDataDisplayed.size(); j++) {
					String expectedValue = myAppsListColumnDataDisplayed.get(j).getText().trim().toUpperCase();
					if (expectedValue.contains("(")) {
						expectedValue = expectedValue.substring(0, expectedValue.indexOf("(") - 1);
					}
					if (expectedValue.contains(".")) {
						expectedValue = expectedValue.substring(0, expectedValue.indexOf(".") - 1);
					}
					if (!actualValue.contains(expectedValue)) {
						Util.addExceptionToReport("Filtering of loans failed", actualValue, expectedValue);
					}
				}
			}
		}
	}

	/**
	 * Verify the pagination
	 * 
	 * @param value
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 * @throws InterruptedException
	 */
	public void verifyPagination(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(value).split(":");
		Select showOptions = new Select(getElementByUsing(args[0]));
		int pagesToCheck;
		if (args.length > 6) {
			pagesToCheck = 3;
		} else {
			pagesToCheck = showOptions.getOptions().size();
		}
		Thread.sleep(2000);
		for (int i = 0; i < pagesToCheck; i++) {
			waitForElement(getElementByUsing(args[1]), 100);
			showOptions.selectByIndex(i);
			Thread.sleep(2000);
			int previousPageRowsCount = 1;
			int noOfPages = Integer.parseInt(args[5]);
			for (int k = 1; k <= noOfPages; k++) {
				String actualInfoShowingBelowTable = getElementByUsing(args[1]).getText();
				String totalApplicationsInMyAppsStr = actualInfoShowingBelowTable.substring(
						actualInfoShowingBelowTable.indexOf("of") + 3,
						actualInfoShowingBelowTable.indexOf("entries") - 1);

				float totalApplicationsInMyApps = Float.parseFloat(totalApplicationsInMyAppsStr.replaceAll(",", ""));

				float entries;

				int noOfRowsDisplayedOnPage = DriverFactory.getDriver().findElements(By.xpath(args[3])).size();
				float showDropdownSelectedValue = Float
						.parseFloat(showOptions.getFirstSelectedOption().getText().trim());
				if (k == (Math.ceil(totalApplicationsInMyApps / showDropdownSelectedValue))) {
					entries = totalApplicationsInMyApps;
				} else {
					entries = (noOfRowsDisplayedOnPage * k);
				}
				String expectedShowingInfoDisplayedBelowTable = null;
				if (showDropdownSelectedValue <= totalApplicationsInMyApps) {
					expectedShowingInfoDisplayedBelowTable = "Showing " + previousPageRowsCount + " to "
							+ String.valueOf((int) entries) + " of " + totalApplicationsInMyAppsStr
							+ " entries\nPlease search date fields in the format MM/DD/YYYY.";

					if (showDropdownSelectedValue < noOfRowsDisplayedOnPage) {
						Util.addExceptionToReport("Number of applications selected to display are not matching",
								noOfRowsDisplayedOnPage + "", showDropdownSelectedValue + "");
					}

					if (!expectedShowingInfoDisplayedBelowTable.contains(actualInfoShowingBelowTable)) {
						Util.addExceptionToReport(
								"First page: Message displayed to show the number of records does not contains the correct count",
								actualInfoShowingBelowTable, expectedShowingInfoDisplayedBelowTable);
					}

					if ((k < noOfPages) && (showDropdownSelectedValue < totalApplicationsInMyApps)) {
						driver.findElement(By.xpath(args[4] + "[" + (k + 1) + "]")).click();
						Thread.sleep(2000);
						waitForElement(getElementByUsing(args[2]), 20);
					}
					previousPageRowsCount = previousPageRowsCount + noOfRowsDisplayedOnPage;

				} else {
					expectedShowingInfoDisplayedBelowTable = "Showing " + previousPageRowsCount + " to "
							+ String.valueOf((int) entries) + " of " + totalApplicationsInMyAppsStr
							+ " entries\nPlease search date fields in the format MM/DD/YYYY.";

					if (noOfRowsDisplayedOnPage != totalApplicationsInMyApps) {
						Util.addExceptionToReport("Number of applications selected to display are not matching",
								noOfRowsDisplayedOnPage + "", showDropdownSelectedValue + "");
					}

					if (!expectedShowingInfoDisplayedBelowTable.contains(actualInfoShowingBelowTable)) {
						Util.addExceptionToReport(
								"First page: Message displayed to show the number of records does not contains the correct count",
								actualInfoShowingBelowTable, expectedShowingInfoDisplayedBelowTable);
					}
				}
			}

			if (args.length > 6) {
				getElementByUsing(args[6]).click();
				Thread.sleep(2000);
			}
		}
	}

	public void verifyPaginationForWebApps(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(value).split(":");
		Select showOptions = new Select(getElementByUsing(args[0]));
		int pagesToCheck;
		if (args.length > 6) {
			pagesToCheck = 3;
		} else {
			pagesToCheck = showOptions.getOptions().size();
		}

		String elementsmMessage = getElementByUsing("MYC_WebApps_paginationrecords").getText();

		int totalNoOfRowsOnPage = Integer.parseInt(
				elementsmMessage.substring(elementsmMessage.indexOf("of") + 1, elementsmMessage.indexOf("entries")));
		String actualInfoShowingBelowTable = getElementByUsing(args[1]).getText();
		String totalApplicationsInMyAppsStr = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf("of") + 3, actualInfoShowingBelowTable.indexOf("entries") - 1);

		float totalApplicationsInMyApps = Float.parseFloat(totalApplicationsInMyAppsStr.replaceAll(",", ""));

		float entries;

		int noOfRowsDisplayedOnPage = DriverFactory.getDriver().findElements(By.xpath(args[3])).size();
		System.out.println(" totalNoOfRowsOnPage ***********************? " + totalNoOfRowsOnPage);
		for (int i = 0; i < pagesToCheck; i++) {
			waitForElement(getElementByUsing(args[1]), 100);
			showOptions.selectByIndex(i);
			float showDropdownSelectedValue = Float.parseFloat(showOptions.getFirstSelectedOption().getText().trim());
			Thread.sleep(2000);
			int previousPageRowsCount = 1;
			int noOfPages = Integer.parseInt(args[5]);
			System.out.println("noOfPages : " + noOfPages);
			// int noOfRowsDisplayed =
			// DriverFactory.getDriver().findElements(By.xpath(args[3])).size();
			float showDropdownSelectedOptions = Float.parseFloat(showOptions.getFirstSelectedOption().getText().trim());

			if (showDropdownSelectedOptions < totalNoOfRowsOnPage) {
				for (int k = 1; k <= noOfPages; k++) {

					System.out.println(
							"noOfRowsDisplayedOnPage + K -------------> " + noOfRowsDisplayedOnPage + " K " + k);

					if (k == (Math.ceil(totalApplicationsInMyApps / showDropdownSelectedValue))) {
						entries = totalApplicationsInMyApps;
					} else {
						entries = (noOfRowsDisplayedOnPage * k);
					}

					System.out.println("entries :: " + entries);
					String expectedShowingInfoDisplayedBelowTable = null;
					if (showDropdownSelectedValue <= totalApplicationsInMyApps) {
						expectedShowingInfoDisplayedBelowTable = "Showing " + previousPageRowsCount + " to "
								+ String.valueOf((int) entries) + " of " + totalApplicationsInMyAppsStr
								+ " entries\nPlease search date fields in the format MM/DD/YYYY.";

						if (showDropdownSelectedValue < noOfRowsDisplayedOnPage) {
							Util.addExceptionToReport("Number of applications selected to display are not matching",
									noOfRowsDisplayedOnPage + "", showDropdownSelectedValue + "");
						}

						if (!expectedShowingInfoDisplayedBelowTable.contains(actualInfoShowingBelowTable)) {
							Util.addExceptionToReport(
									"First page: Message displayed to show the number of records does not contains the correct count",
									actualInfoShowingBelowTable, expectedShowingInfoDisplayedBelowTable);
						}

						if ((k < noOfPages) && (showDropdownSelectedValue < totalApplicationsInMyApps)) {
							driver.findElement(By.xpath(args[4] + "[" + (k + 1) + "]")).click();
							Thread.sleep(2000);
							waitForElement(getElementByUsing(args[2]), 20);
						}
						previousPageRowsCount = previousPageRowsCount + noOfRowsDisplayedOnPage;

					} else {
						expectedShowingInfoDisplayedBelowTable = "Showing " + previousPageRowsCount + " to "
								+ String.valueOf((int) entries) + " of " + totalApplicationsInMyAppsStr
								+ " entries\nPlease search date fields in the format MM/DD/YYYY.";

						if (noOfRowsDisplayedOnPage != totalApplicationsInMyApps) {
							Util.addExceptionToReport("Number of applications selected to display are not matching",
									noOfRowsDisplayedOnPage + "", showDropdownSelectedValue + "");
						}

						if (!expectedShowingInfoDisplayedBelowTable.contains(actualInfoShowingBelowTable)) {
							Util.addExceptionToReport(
									"First page: Message displayed to show the number of records does not contains the correct count",
									actualInfoShowingBelowTable, expectedShowingInfoDisplayedBelowTable);
						}
					}
				}
			} else {
				if (showDropdownSelectedValue < noOfRowsDisplayedOnPage) {
					Util.addExceptionToReport("Number of applications selected to display are not matching",
							noOfRowsDisplayedOnPage + "", showDropdownSelectedValue + "");
				}
			}

			if (args.length > 6) {
				getElementByUsing(args[6]).click();
				Thread.sleep(2000);
			}
		}
	}

	/**
	 * Get date based on the input date format and the number of days
	 * 
	 * @author muni.reddy
	 * @param dateFormat
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws BiffException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public String enterEmployementDateStarted(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("****** enterEmployementDateStarted *************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		String emplymentStartDate = Util.getDateToSearch(args[0], args[1], Integer.parseInt(args[2]), args[3]);
		System.out.println("emplymentStartDate : " + emplymentStartDate);
		getElementByUsing(args[4]).clear();
		Thread.sleep(1000);
		getElementByUsing(args[4]).sendKeys(emplymentStartDate);

		System.out.println("emplymentStartDate : " + emplymentStartDate);

		return emplymentStartDate;
	}

	/**
	 * Expands / Collapse the web element based on requirement
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public void expandOrCollapseTenOfThreeSection(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		String[] args = KWVariables.getVariables().get(values).split(":");
		String currentType = getElementByUsing(args[0]).getAttribute(args[1]);

		if (!currentType.isEmpty() && currentType.length() > 1) {
			currentType = currentType.split(" ")[1];
		}
		if (!currentType.equalsIgnoreCase(args[2])) {
			getElementByUsing(args[0]).click();
			Thread.sleep(5000);
		}
	}

	/**
	 * Will wait for element visibility till the specified time with pooling of
	 * one second
	 * 
	 * @author muni.reddy
	 * @param element
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 */
	public boolean waitTillElementToDisappear(String element, int waitTimeInSeconds)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		boolean displayed = true;
		int maxtime = 0;
		boolean isVisible = true;
		while (displayed) {
			isVisible = getElementByUsing(element).isDisplayed();
			if (!isVisible) {
				break;
			} else if (maxtime < waitTimeInSeconds) {
				maxtime = maxtime + 1;
				Thread.sleep(1000);
			}
		}
		return isVisible;
	}

	/**
	 * Waits for element to disappear
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void waitForElementToDisappear(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String args[] = values.split(",");
		boolean isElementDisplayed = waitTillElementToDisappear(args[0], elementDisplayTimeOut);
		if (isElementDisplayed) {
			System.out.println("Element displayed : " + isElementDisplayed);
		} else {
			System.out.println("Element disappeared : " + isElementDisplayed);
		}
	}

	/**
	 * Verifies the sorting of column data in ascending and descending orders
	 * 
	 * @author muni.reddy
	 * @param columnnnumber
	 *            to be sorted
	 * @throws TwfException
	 * @throws InterruptedException
	 * @throws BiffException
	 * @throws IOException
	 */
	public void verifySortingOfItemsDisplayed(String valuesTps)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		driver = DriverFactory.getDriver();
		String values[] = KWVariables.getVariables().get(valuesTps).split(":");
		String tableHeaderPreXpath = values[0];
		String tableHeaderPostXpath = values[1];
		String tableBodyPreXpath = values[2];
		String tableBodyPostXpath = values[3];
		String pageNumXpath = values[5];
		String columnNo = values[6];

		List<String> unsortedList = new ArrayList<String>();
		List<String> afterSortingcolumnFirstList = new ArrayList<String>();
		List<String> afterSortingcolumnSecondList = new ArrayList<String>();
		waitTillElementToDisappear(values[4], 30);
		int noofPages = driver.findElements(By.xpath(pageNumXpath)).size();
		String nextpage = "";
		for (int i = 1; i <= noofPages; i++) {
			unsortedList.addAll(getListOfItemsDisplaying(tableBodyPreXpath, tableBodyPostXpath, columnNo));
			if ((i + 1) <= noofPages) {
				nextpage = pageNumXpath + "[" + (i + 1) + "]";
				driver.findElement(By.xpath(nextpage)).click();
			}
			Thread.sleep(3000);
		}
		List<String> blanklist = new ArrayList<>();
		List<String> ascendingOrder = new ArrayList<>();
		List<String> descscendingOrder = new ArrayList<>();
		List<Integer> loanamount = new ArrayList<>();
		NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "US"));
		// sorting of Currency Columns
		if (columnNo.equals("3")) {
			for (int i = 0; i < unsortedList.size(); i++) {
				loanamount.add(Integer.parseInt(unsortedList.get(i).replace("$", "").replaceAll(",", "").trim()));
			}
			Collections.sort(loanamount);
			// Collections.reverse(loanamount);
			unsortedList.clear();
			for (int i = 0; i < loanamount.size(); i++) {
				nf.setMaximumFractionDigits(0);
				unsortedList.add(nf.format(loanamount.get(i)).toString());
			}
			if (unsortedList.size() >= 100) {
				ascendingOrder = unsortedList.subList(0, 100);
			} else {
				ascendingOrder = unsortedList;
			}
		} else {
			for (int i = 0; i < unsortedList.size(); i++) {
				if (unsortedList.get(i).equalsIgnoreCase("N/A")) {
					blanklist.add(unsortedList.get(i));
				}
			}
			if (blanklist.size() > 0) {
				unsortedList.removeAll(blanklist);
			}
			Collections.sort(unsortedList);
			if (blanklist.size() >= 100) {
				ascendingOrder.addAll(blanklist);
				ascendingOrder.addAll(unsortedList);

				ascendingOrder = ascendingOrder.subList(0, 100);

			} else {
				// ascendingOrder=blanklist;
				ascendingOrder.addAll(blanklist);
				ascendingOrder.addAll(unsortedList);

			}
		}

		clickOnColumnHeader(tableHeaderPreXpath, tableHeaderPostXpath, columnNo);
		Thread.sleep(5000);
		afterSortingcolumnFirstList = getListOfItemsDisplaying(tableBodyPreXpath, tableBodyPostXpath, columnNo);
		Thread.sleep(2000);
		if (!ascendingOrder.equals(afterSortingcolumnFirstList)) {
			Util.addExceptionToReport("Sorting of ascending order: ", afterSortingcolumnFirstList + "",
					ascendingOrder + "");
		}
		Collections.reverse(unsortedList);
		if (blanklist.size() > 0) {
			unsortedList.addAll(blanklist);
		}
		if (unsortedList.size() >= 100) {
			descscendingOrder = unsortedList.subList(0, 100);
		} else {
			descscendingOrder = unsortedList;
		}

		clickOnColumnHeader(tableHeaderPreXpath, tableHeaderPostXpath, columnNo);
		Thread.sleep(5000);
		afterSortingcolumnSecondList = getListOfItemsDisplaying(tableBodyPreXpath, tableBodyPostXpath, columnNo);
		if (!descscendingOrder.equals(afterSortingcolumnSecondList)) {
			Util.addExceptionToReport("Sorting of Descending order: ", afterSortingcolumnSecondList + "",
					descscendingOrder + "");
		}

	}

	/**
	 * Displays all the items displayed in the dash board
	 * 
	 * @param columnn
	 *            number to be sorted
	 * 
	 * @throws TwfException
	 * 
	 * @throws InterruptedException
	 */

	public List<String> getListOfItemsDisplaying(String tableHeaderPreXpath, String tableBodyPostXpath, String columnNo)
			throws TwfException, InterruptedException {
		WebDriver driver = DriverFactory.getDriver();
		List<WebElement> elemnets;
		int itemIndex = 1;
		String Id;
		List<String> unsortedList = new ArrayList<String>();
		elemnets = driver.findElements(By.xpath(tableHeaderPreXpath + tableBodyPostXpath + "[" + columnNo + "]"));
		int numberOfRowsDisplayed = elemnets.size();
		for (int i = 1; i <= (numberOfRowsDisplayed); i++) {
			WebElement we = driver.findElement(
					By.xpath(tableHeaderPreXpath + "[" + itemIndex + "]" + tableBodyPostXpath + "[" + columnNo + "]"));
			if (columnNo.equals("1") || columnNo.equals("7")) {
				Id = we.getText().split(",")[0];
			} else {
				Id = we.getText();
			}
			unsortedList.add(Id);
			if (itemIndex < numberOfRowsDisplayed) {
				itemIndex++;
			}
		}
		return unsortedList;
	}

	/**
	 * Clicks on the column number to sort
	 * 
	 * @author muni.reddy
	 * @param columnn
	 *            number to be sorted
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void clickOnColumnHeader(String tableHeaderPreXpath, String tableHeaderPostXpath, String columnNumber)
			throws TwfException, InterruptedException {
		WebDriver driver = DriverFactory.getDriver();
		WebElement columnHeader = driver
				.findElement(By.xpath(tableHeaderPreXpath + tableHeaderPostXpath + "[" + columnNumber + "]"));
		columnHeader.click();
		Thread.sleep(6000);
	}

	/**
	 * Verifies the links names
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyMICompaniesLinkNames(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("**************** verifyMICompaniesLinkNames ********************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		WebDriver driver = DriverFactory.getDriver();
		List<WebElement> linksList = driver.findElements(By.xpath(args[0]));
		String expectedMICompanyName = "";
		String actualMICompanyName = "";
		String actualMICompanyNamesMismatch = "";
		String expectedMICompanyNamesMismatch = "";
		boolean isMismatch = false;
		for (int i = 0; i < linksList.size(); i++) {
			actualMICompanyName = linksList.get(i).getText().trim();
			expectedMICompanyName = args[i + 1];
			if (!expectedMICompanyName.equalsIgnoreCase(actualMICompanyName)) {
				actualMICompanyNamesMismatch = actualMICompanyNamesMismatch + expectedMICompanyName;
				expectedMICompanyNamesMismatch = expectedMICompanyNamesMismatch + expectedMICompanyName;
			}
			System.out.println("expectedMICompanyName : actualMICompanyName  ==> " + expectedMICompanyName + " : "
					+ actualMICompanyName);
		}

		if (isMismatch) {
			Util.addExceptionToReport("MI Companies Names mismatch:", actualMICompanyNamesMismatch,
					expectedMICompanyName);
		}
	}

	/**
	 * Verifies pushing app to MyKey application
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	
	
	public void verifySendtoMyKeyFunctionality(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {

		String args[] = KWVariables.getVariables().get(values).split(",");
		String quickApp_section_info = getElementByUsing(args[0]).getText();
		String appId = quickApp_section_info.substring(quickApp_section_info.lastIndexOf("ID") + 3,
				quickApp_section_info.length());
		String actualWarning_info = getElementByUsing(args[1]).getText();
		String expectedWarningMsg = args[5].replaceAll(args[7], appId).trim();
		if (!expectedWarningMsg.equalsIgnoreCase(actualWarning_info)) 
		{
			Util.addExceptionToReport("Warning info mismatch", actualWarning_info, expectedWarningMsg);
		} 
		
		else {
			getElementByUsing(args[2]).click();
			Thread.sleep(2000);
			//getElementByUsing(args[3]).click();
			waitForElement(getElementByUsing(args[4]), elementDisplayTimeOut);
			
			String finalSendSuccessInfo = getElementByUsing(args[4]).getText().trim();
			String expectedStr = args[6].replaceAll(args[7], appId).trim();

			if (!expectedStr.equalsIgnoreCase(finalSendSuccessInfo)) {
				Util.addExceptionToReport("Warning info mismatch", finalSendSuccessInfo, expectedStr);
			}
		}
	}

	/**
	 * Get quick app id and store it for future usage
	 * 
	 * @author muni.reddy
	 * @param values
	 * @return
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public String getQuickAppId(String values) throws BiffException, IOException, TwfException, InvalidFormatException {
		if (getElementByUsing(values).isDisplayed()) {
			String quickSAppIdLocal = getElementByUsing(values).getText().trim();
			quickAppId = quickSAppIdLocal.substring(quickSAppIdLocal.indexOf("ID") + 3, quickSAppIdLocal.length());
		}
		return quickAppId;
	}

	/**
	 * Verifies mandatory fields error message
	 * 
	 * @author muni.reddy
	 * @param dataPoolArgs
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyMandatoryFieldsErrors(Map<String, String> dataPoolArgs)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		driver = DriverFactory.getDriver();
		String elements[] = dataPoolArgs.get("RepoElements").split(",");
System.out.println("-----------------verifyMandatoryFieldsErrors------------"+elements[0]);
		getElementByUsing(elements[0]).click();
		Thread.sleep(2000);

		String localDataColumns[] = dataPoolArgs.get("DataColumns").split(",");
		for (int k = 0; k< localDataColumns.length; k++) 
		System.out.println(elements[k]);

		for (int i = 0; i < localDataColumns.length; i++) {
			String values[] = dataPoolArgs.get(localDataColumns[i]).split("::");
			getElementByUsing(elements[1]).clear();
			getElementByUsing(elements[1]).sendKeys(values[2].trim());

			getElementByUsing(elements[2]).clear();
			getElementByUsing(elements[2]).sendKeys(values[3].trim());

			getElementByUsing(elements[3]).clear();
			getElementByUsing(elements[3]).sendKeys(values[4].trim());

			getElementByUsing(elements[4]).clear();
			getElementByUsing(elements[4]).sendKeys(values[5].trim());

			if (!values[6].trim().isEmpty()) {
				new Select(getElementByUsing(elements[5])).selectByValue(values[6]);
			}
			if (!values[7].trim().isEmpty()) {
				new Select(getElementByUsing(elements[6])).selectByValue(values[7]);
			}
			if (!values[8].trim().isEmpty()) {
				new Select(getElementByUsing(elements[7])).selectByValue(values[8]);
			}

			if (!values[9].trim().isEmpty()) {
				getElementByUsing(elements[10]).clear();
				getElementByUsing(elements[10]).sendKeys(values[9].trim());
			}

			if (!values[10].trim().isEmpty()) {
				getElementByUsing(elements[11]).clear();
				getElementByUsing(elements[11]).sendKeys(values[10].trim());
			}

			if (!values[11].trim().isEmpty()) {
				getElementByUsing(elements[12]).clear();
				getElementByUsing(elements[12]).sendKeys(values[11].trim());
			}

			if (!values[12].trim().isEmpty()) {
				getElementByUsing(elements[13]).clear();
				getElementByUsing(elements[13]).sendKeys(values[12].trim());
			}

			Thread.sleep(1000);
			getElementByUsing(elements[8]).click();
			Thread.sleep(2000);
			getQuickAppId(elements[9]);

			String expectedErrorMessage = values[0];

			if (quickAppId != null && quickAppId.length() > 1) {
				expectedErrorMessage = expectedErrorMessage.replaceAll("dynamicAppId", quickAppId);
			}

			List<WebElement> errorsList = driver.findElements(By.xpath(values[1]));
			String actualErrorMsg = "";
			for (int j = 0; j < errorsList.size(); j++) {
				actualErrorMsg = actualErrorMsg + errorsList.get(j).getText().trim() + "\n";
			}

			actualErrorMsg = actualErrorMsg.trim();
			expectedErrorMessage = expectedErrorMessage.trim();

			if (!expectedErrorMessage.equalsIgnoreCase(actualErrorMsg)) {
				Util.addExceptionToReport("Warning info mismatch", actualErrorMsg, expectedErrorMessage);
			}
		}
	}

	/**
	 * Verifies LTV, CLTV values
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void verifyLTV_CLTV_Value(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		String expected_ltv_cltvString = "";
		float expected_ltv_cltv = 0;
		String args[] = KWVariables.getVariables().get(values).split(":");
		String purposeOfLoan = args[0].trim();

		float estimatedValue = Float
				.parseFloat(getElementByUsing(args[1]).getAttribute("value").replaceAll(",", "").trim());

		float baseLoanAmount = Float
				.parseFloat(getElementByUsing(args[2]).getAttribute("value").replaceAll(",", "").trim());

		String actual_ltvStr = getElementByUsing(args[3]).getText().split("%/")[0] + "%";
		DecimalFormat df = new DecimalFormat(".000");

		if (!purposeOfLoan.equalsIgnoreCase("Purchase")) {
			expected_ltv_cltv = (baseLoanAmount / estimatedValue) * 100;
			expected_ltv_cltvString = expected_ltv_cltv + "";
			expected_ltv_cltvString = df.format(expected_ltv_cltv) + "%";
		} else {
			float salesPrice = Float
					.parseFloat(getElementByUsing(args[4]).getAttribute("value").replaceAll(",", "").trim());
			if (estimatedValue > salesPrice) {
				expected_ltv_cltv = (baseLoanAmount / salesPrice) * 100;
			} else {
				expected_ltv_cltv = (baseLoanAmount / estimatedValue) * 100;
			}
			expected_ltv_cltvString = expected_ltv_cltv + "";
			expected_ltv_cltvString = df.format(expected_ltv_cltv) + "%";
		}

		if (!expected_ltv_cltvString.equalsIgnoreCase(actual_ltvStr)) {
			//Util.addExceptionToReport("LTV and CLTV values mismatch", actual_ltvStr, expected_ltv_cltvString);
		}
	}

	/**
	 * Verifies the respa date
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void verifyRespaDate(String values) throws BiffException, IOException, TwfException, InvalidFormatException {
		respaDate = "";
		String args[] = KWVariables.getVariables().get(values).split(",");
		String createdDate = getElementByUsing(args[0]).getText().trim();
		String respaDateStr = getElementByUsing(args[1]).getText().trim();

		if (!respaDateStr.equalsIgnoreCase(args[2])) {
			respaDate = respaDateStr.substring(respaDateStr.indexOf("/") - 2, respaDateStr.lastIndexOf("/") + 5);
		} else {
			respaDate = respaDateStr.substring(respaDateStr.indexOf(args[3]), respaDateStr.length());
		}

		if (!args[3].equalsIgnoreCase(args[4]) && respaDateStr.contains(args[3])) {
			System.out.println("******************** First if ********************");
			//Util.addExceptionToReport("RESPA Date mismatch", respaDate,
			//args[2]);
		} else if (args[3].equalsIgnoreCase(args[4]) && !respaDateStr.equalsIgnoreCase(args[2])) {
			// System.out.println("******************** Second if
			// ********************");
			Util.addExceptionToReport("RESPA Date mismatch", respaDate, createdDate);
		}
	}

	// This method written to push app id to mykey, we have to remove this
	// method once the SendToMyKey button functionality works fine
	public void pushAppIdToMyKey(String values)
			throws BiffException, IOException, TwfException, InterruptedException, InvalidFormatException {
		System.out.println("**************** pushAppIdToMyKey ******************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		WebDriver driver = DriverFactory.getDriver();
		System.out.println("args[0] --> " + args[0]);
		// driver.navigate().to("https://mycircle-qa-08.guildmortgage.com/myapps/data-push/test.php");
		String url = args[0].replace("login?", "");
		System.out.println(" url ---> " + url);
		driver.navigate().to(url);
		Util.switchToWindow(driver);
		System.out.println("quickAppId :: " + quickAppId);
		waitForElement(getElementByUsing(args[1]), 120);
		getElementByUsing(args[1]).sendKeys(quickAppId);
		Thread.sleep(2000);
		getElementByUsing(args[2]).click();
		Thread.sleep(2000);
		getElementByUsing(args[3]).click();
		Thread.sleep(20000);
		driver.navigate().back();
		waitForElement(getElementByUsing(args[3]), 60);
		driver.navigate().back();
	}

	/**
	 * Get application id generated and store it for future usage
	 * 
	 * @author muni.reddy
	 * @param value
	 * @return
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public String getApplicationNumber(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		String args = KWVariables.getVariables().get(value);
		String successInfo = getElementByUsing(args).getText().trim();
		if (successInfo != null && successInfo.length() > 10) {
			applicationId = successInfo.substring(successInfo.indexOf("ID") + 3, successInfo.length());
		}
		System.out.println("applicationId ==> " + applicationId);
		return applicationId;
	}

	/**
	 * Search dynamically generated application id
	 * 
	 * @author muni.reddy
	 * @param values
	 * @return
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public String searchApplication(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String args[] = KWVariables.getVariables().get(values).split(":");
		waitForElement(getElementByUsing(args[0]), 60);
		getElementByUsing(args[0]).clear();
		getElementByUsing(args[0]).sendKeys(applicationId);
		waitForElement(getElementByUsing(args[1]), 60);
		getElementByUsing(args[1]).click();
		waitForElement(getElementByUsing(args[2]), 60);
		getElementByUsing(args[2]).click();
		waitForElement(getElementByUsing(args[3]), 120);
		return getElementByUsing(args[3]).getText().trim();
	}

	/**
	 * Verify deleted application
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void verifySearchAfterDelete(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String[] args = KWVariables.getVariables().get(values).split(":");
		if (args[5].equalsIgnoreCase("nonexistance")) {
			Util.addExceptionToReport("Delete application", searchApplication(values), args[5].trim());
		}
	}

	/**
	 * Creates loans CSV file
	 * 
	 * @author muni.reddy
	 * @param args
	 * @throws IOException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws TwfException
	 */
	public void createLoansCSVFile(String args)
			throws IOException, BiffException, InvalidFormatException, TwfException {
		applicationValues = new ArrayList<String>();
		String[] columnNames = KWVariables.getVariables().get(args).split(",")[0].split(":");
		String[] columnValues = KWVariables.getVariables().get(args).split(",")[1].split(":");
		String quickAppRepoId = KWVariables.getVariables().get(args).split(",")[2].trim();
		String[] fileToCreateDetails = KWVariables.getVariables().get(args).split(",")[3].split(":");
		FileWriter writer = null;
		String resultFolderDate = new SimpleDateFormat(fileToCreateDetails[0]).format(new Date())
				.replaceAll("[\\/\\-\\:]", "_");

		File currentResDir = new File(System.getProperty("user.dir") + File.separator + fileToCreateDetails[1]
				+ File.separator + fileToCreateDetails[2] + resultFolderDate);
		currentResDir.mkdirs();
		String newLoansCreatedCSVFilePath = currentResDir.getAbsolutePath() + File.separator + fileToCreateDetails[3];
		quickAppId = getQuickAppId(quickAppRepoId);
		applicationValues.add(quickAppId);
		applicationValues.add(borrowerName.split(" ")[0]);
		applicationValues.add(borrowerName.split(" ")[1]);
		if (applicationId != null) {
			applicationValues.add(applicationId);

		} else {
			applicationId = "AppId_Not_generated";
			applicationValues.add(applicationId);

		}

		for (int j = 0; j < columnValues.length; j++) {
			String actualValue = getElementByUsing(columnValues[j]).getText().replaceAll(",", "").trim();
			if (actualValue.contains("RESPA Date")) {
				actualValue = actualValue.substring(actualValue.indexOf("/") - 2, actualValue.length());
			}
			applicationValues.add(actualValue);
		}

		try {
			boolean alreadyExists = new File(newLoansCreatedCSVFilePath).exists();
			if (!alreadyExists) {
				writer = new FileWriter(newLoansCreatedCSVFilePath, true);
				for (int i = 0; i < columnNames.length; i++) {
					writer.write(columnNames[i]);
					writer.write(",");
				}
				writer.write("\n");

			} else {
				writer = new FileWriter(newLoansCreatedCSVFilePath, true);
			}

			for (int i = 0; i < applicationValues.size(); i++) {
				writer.write(applicationValues.get(i).toString().trim());
				writer.write(",");
			}
			writer.write("\n");
			System.out.println("Write success!");
		} catch (IOException e) {
			e.printStackTrace();
			writer.close();
		} finally {
			writer.close();
		}
	}

	/**
	 * Verifies the total origination fee value
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void verifyTotalOriginationFee(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		float expectedTotalOrgFee = 0;
		String args[] = KWVariables.getVariables().get(values).split(",");
		String var[] = args[0].split(":");
		String parm[] = args[1].split(":");

		String baseLoanAmtStr = getElementByUsing(parm[0]).getText().replaceAll("$", "").replaceAll(",", "");
		baseLoanAmtStr = baseLoanAmtStr.substring(1, baseLoanAmtStr.length());
		float expectedTotalOriginationFee = 0;
		String purposeOfLoan = getElementByUsing(var[0]).getText();

		String valueOriginationFee = "";
		if (purposeOfLoan.equalsIgnoreCase(parm[2])) {

			for (int i = 1; i < var.length; i++) {
				valueOriginationFee = getElementByUsing(var[i]).getAttribute(parm[3]).trim();
				Thread.sleep(500);
				if (!valueOriginationFee.isEmpty()) {
					expectedTotalOriginationFee = expectedTotalOriginationFee
							+ Float.parseFloat(valueOriginationFee.replaceAll(",", "").replaceAll("%", "").trim());

				}

			}
			getElementByUsing(parm[1]).click();
			expectedTotalOrgFee = (Float.parseFloat(baseLoanAmtStr) * expectedTotalOriginationFee) / 100;
			String actualOrgFeStr = getElementByUsing(parm[1]).getText().replaceAll("$", "").replaceAll(",", "").trim();
			float actualTotalOrgFee = Float.parseFloat(actualOrgFeStr.substring(1, actualOrgFeStr.length()));
			if (expectedTotalOrgFee != actualTotalOrgFee) {
				Util.addExceptionToReport("ORIGINATION FEE Totla mismath", actualTotalOrgFee + "",
						expectedTotalOrgFee + "");
			}
		}
	}

	/**
	 * Verifies the appraisal fee value
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void verifyTotalAppraisalFee(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String args[] = KWVariables.getVariables().get(values).split(",");
		String var[] = args[0].split(":");
		String parms[] = args[1].split(":");
		float expectedTotalAppraisalFee = 0;
		int i = 0;

		String purposeOfLoan = getElementByUsing(parms[0]).getText().trim();
		String valueAppraisalFee = "";

		if (!purposeOfLoan.equalsIgnoreCase(parms[2])) {
			i = 1;
		}
		for (; i < var.length; i++, i++) {
			valueAppraisalFee = getElementByUsing(var[i]).getAttribute(parms[5]).trim();
			Thread.sleep(500);
			if (!valueAppraisalFee.isEmpty()) {
				expectedTotalAppraisalFee = expectedTotalAppraisalFee + Float.parseFloat(valueAppraisalFee
						.substring(1, valueAppraisalFee.length()).replaceAll(",", "").replaceAll("$", "").trim());
			}
		}
		getElementByUsing(var[2]).click();

		String actualTotalAppraisalFeeStr = getElementByUsing(parms[3]).getText().replaceAll("$", "")
				.replaceAll(",", "").trim();
		float actualTotalAppraisalFee = Float
				.parseFloat(actualTotalAppraisalFeeStr.substring(1, actualTotalAppraisalFeeStr.length()));

		if (expectedTotalAppraisalFee != actualTotalAppraisalFee) {
			System.out.println("expectedTotalOrgFee!=actualTotalOrgFee ==> " + expectedTotalAppraisalFee + " != "
					+ actualTotalAppraisalFee);
		}
	}

	/**
	 * Verify the default fee values
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyDefaultFeeValues(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		WebDriver driver = DriverFactory.getDriver();
		ArrayList<String> defaultValuesList = new ArrayList<String>();
		ArrayList<String> buyerValuesList = new ArrayList<String>();
		ArrayList<String> non_Recurring_Default_InformationValuesList = new ArrayList<String>();
		String defaultValue = "";
		String buyerValue = "";
		String non_Recurring_Default_InformationValue = "";
		int j = 1;

		String loanProgram = getElementByUsing(args[0]).getText().trim();
		List<WebElement> non_Recurring_Default_InformationWList = driver.findElements(By.xpath(args[2]));
		List<WebElement> defaultWList = driver.findElements(By.xpath(args[3]));

		List<WebElement> byerWList = driver.findElements(By.xpath(args[4]));

		if (loanProgram.contains(args[1])) {
			defaultValuesList.add(defaultWList.get(0).getText().trim());
			j = 2;
		}
		for (int i = 0; i < non_Recurring_Default_InformationWList.size(); i++) {
			if (non_Recurring_Default_InformationWList.get(i) != null
					&& non_Recurring_Default_InformationWList.get(i).getAttribute(args[5]) != args[6]) {
				non_Recurring_Default_InformationValue = non_Recurring_Default_InformationWList.get(i).getText().trim();
				non_Recurring_Default_InformationValuesList.add(non_Recurring_Default_InformationValue);
			}
		}

		for (; j < defaultWList.size(); j++) {
			if (defaultWList.get(j) != null && defaultWList.get(j).getAttribute(args[5]) != args[6]
					&& !defaultWList.get(j).getText().equals("-")) {
				defaultValue = defaultWList.get(j).getText().trim();
				defaultValuesList.add(defaultValue);
			}
		}

		for (int k = 0; k < byerWList.size(); k++) {
			if (byerWList.get(k) != null && byerWList.get(k).getAttribute(args[5]) != args[6]
					&& byerWList.get(k).getAttribute(args[7]).length() > 0
					&& !byerWList.get(k).getAttribute(args[7]).isEmpty()) {
				buyerValue = byerWList.get(k).getAttribute(args[7]).trim();
				buyerValuesList.add(buyerValue);
			}
		}

		if (!non_Recurring_Default_InformationValuesList.equals(defaultValuesList)) {
			Util.addExceptionToReport("Non Recurring Default InformationValues values mismath",
					non_Recurring_Default_InformationValuesList + "", defaultValuesList + "");
		}
	}

	/**
	 * Verifies the default static information
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InvalidFormatException
	 */
	public void verifyDefaultFeeInformation(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("********************** verifyDefaultFeeInformation ************************");
		String args[] = KWVariables.getVariables().get(values).split("::");
		String var[] = args[3].split(",");
		String expectedDefaultInfo = "";
		String actualDefaultInfo = getElementByUsing(args[1]).getText();
		System.out.println("*********** actualDefaultInfo :: " + actualDefaultInfo);
		String option = getElementByUsing(args[2]).getText().split(" ")[0].trim();
		String firstElementToReplace = var[0];
		String secondStringToReplace = var[1];
		String thirdStringToReplace = var[2];
		String fourthStringToReplace = var[3];

		System.out.println("firstElementToReplace -------> " + firstElementToReplace);
		System.out.println("secondStringToReplace -------> " + secondStringToReplace);
		System.out.println("thirdStringToReplace -------> " + thirdStringToReplace);
		System.out.println("fourthStringToReplace -------> " + fourthStringToReplace);

		if (guildToGuildRft) {
			args[0] = args[0].replace("SecondStringToReplace", "$30.00 (Must be exact)");
			guildToGuildRft = false;
		}

		System.out.println("args[0] :: " + args[0]);
		System.out.println("option :::::::::::::::::: " + option);
		switch (option) {

		case "Rural":
			System.out.println("args[4].split(\"#\")[0] -- > " + args[4].split("#")[0]);
			System.out.println("args[4].split(\"#\")[2] -- > " + args[4].split("#")[2]);
			System.out.println("args[4].split(\"#\")[3] -- > " + args[4].split("#")[3]);
			System.out.println("args[4].split(\"#\")[4] -- > " + args[4].split("#")[4]);
			expectedDefaultInfo = args[0].trim();
			expectedDefaultInfo = expectedDefaultInfo.replace(firstElementToReplace, args[4].split("#")[0]);
			expectedDefaultInfo = expectedDefaultInfo.replace(secondStringToReplace, args[4].split("#")[2]);
			expectedDefaultInfo = expectedDefaultInfo.replace(thirdStringToReplace, args[4].split("#")[3]);
			expectedDefaultInfo = expectedDefaultInfo.replace("FourthStringToReplace", args[4].split("#")[4]);
			break;
		case "Va":
			System.out.println("args[4].split(\"#\")[0] -- > " + args[4].split("#")[0]);
			System.out.println("args[4].split(\"#\")[1] -- > " + args[4].split("#")[1]);
			System.out.println("args[4].split(\"#\")[0] -- > " + args[4].split("#")[0]);
			System.out.println("args[4].split(\"#\")[4] -- > " + args[4].split("#")[4]);
			expectedDefaultInfo = args[0].trim();
			expectedDefaultInfo = expectedDefaultInfo.replace(firstElementToReplace, args[4].split("#")[0]);
			expectedDefaultInfo = expectedDefaultInfo.replace(secondStringToReplace, args[4].split("#")[1]);
			expectedDefaultInfo = expectedDefaultInfo.replace(thirdStringToReplace, args[4].split("#")[0]);
			expectedDefaultInfo = expectedDefaultInfo.replace(fourthStringToReplace, args[4].split("#")[4]);
		case "Fha":
			System.out.println("args[4].split(\"#\")[0] -- > " + args[4].split("#")[0]);
			System.out.println("args[4].split(\"#\")[1] -- > " + args[4].split("#")[1]);
			System.out.println("args[4].split(\"#\")[0] -- > " + args[4].split("#")[0]);
			System.out.println("args[4].split(\"#\")[4] -- > " + args[4].split("#")[4]);
			expectedDefaultInfo = args[0].trim();
			expectedDefaultInfo = expectedDefaultInfo.replace(firstElementToReplace, args[4].split("#")[0]);
			expectedDefaultInfo = expectedDefaultInfo.replace(secondStringToReplace, args[4].split("#")[1]);
			expectedDefaultInfo = expectedDefaultInfo.replace(thirdStringToReplace, args[4].split("#")[0]);
			expectedDefaultInfo = expectedDefaultInfo.replace(fourthStringToReplace, args[4].split("#")[4]);
			break;
		case "Fnma":
			System.out.println("args[4].split(\"#\")[6] -- > " + args[4].split("#")[6]);
			System.out.println("args[4].split(\"#\")[1] -- > " + args[4].split("#")[1]);
			System.out.println("args[4].split(\"#\")[0] -- > " + args[4].split("#")[0]);
			System.out.println("args[4].split(\"#\")[5] -- > " + args[4].split("#")[5]);
			expectedDefaultInfo = args[0].trim();
			expectedDefaultInfo = expectedDefaultInfo.replace(firstElementToReplace, args[4].split("#")[6]);
			expectedDefaultInfo = expectedDefaultInfo.replace(secondStringToReplace, args[4].split("#")[1]);
			expectedDefaultInfo = expectedDefaultInfo.replace(thirdStringToReplace, args[4].split("#")[0]);
			expectedDefaultInfo = expectedDefaultInfo.replace(fourthStringToReplace, args[4].split("#")[5]);
			break;
		}

		if (!expectedDefaultInfo.equals(actualDefaultInfo)) {
			Util.addExceptionToReport("Default Fee Information! text mismath", actualDefaultInfo, expectedDefaultInfo);
		}
	}

	/**
	 * Verifies the default information in miscellaneous section
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */

	/*
	 * MYC_Bor_QuickApp_Edit_LoanProgram_value
	 * ::MYC_MyApp_Fee_NR_Closing_Costs_Miscellaneous_Default_values
	 * ::Non-Recurring Misc Default Information!
	 * 
	 * 
	 */
	public void verifyDefaultMiscellaneousInfo(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("-------------verifyDefaultMiscellaneousInfo-------------");
		String args[] = KWVariables.getVariables().get(values).split("::");
		String option = getElementByUsing(args[0]).getText().split(" ")[0].trim();
		String expectedDefaultInfo = "";
		String actualDefaultInfo = getElementByUsing(args[1]).getText();
		switch (option) {
		case "Rural":
			expectedDefaultInfo = args[2];
			break;
		case "Fha":
			expectedDefaultInfo = args[3];
			break;
		case "Va":
			expectedDefaultInfo = args[2];
			break;
		case "Fnma":
			expectedDefaultInfo = args[2];
			break;
		}

		if (!expectedDefaultInfo.equalsIgnoreCase(expectedDefaultInfo)) {
			if (!actualDefaultInfo.contains(actualDefaultInfo)) {
				Util.addExceptionToReport("Non-Recurring Closing Costs Miscellaneous text mismath", actualDefaultInfo,
						expectedDefaultInfo);
			}
		}
	}

	/**
	 * Verifies the ten o three page mandatory errors
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void verifyTenOrThreeErrors(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String args[] = KWVariables.getVariables().get(values).split("::");
		WebDriver driver = DriverFactory.getDriver();
		List<WebElement> errorsList = driver.findElements(By.xpath(args[0]));
		Thread.sleep(2000);
		String actualErrorMsg = "";
		for (int j = 0; j < errorsList.size(); j++) {
			actualErrorMsg = actualErrorMsg + errorsList.get(j).getText().trim() + "\n";
		}
		actualErrorMsg = actualErrorMsg.trim();
		String expectedError = args[1].trim();
		if (!expectedError.equalsIgnoreCase(actualErrorMsg)) {
			Util.addExceptionToReport("Mandatory information missing error info mismatch", actualErrorMsg,
					expectedError);
		}
	}

	/**
	 * Verifies the length of the decimal points
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyLengthOfString(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String elementValue = getElementByUsing(args[0]).getText().trim();
		System.out.println("elementValue  ===> " + elementValue);

		int afterDecimalPointLength = elementValue.substring(elementValue.indexOf(".") + 1, elementValue.length() - 1)
				.length();
		int expectedLength = Integer.parseInt(args[1]);

		System.out.println(
				"afterDecimalPointLength :: expectedLength ===> " + afterDecimalPointLength + " : " + expectedLength);
		if (expectedLength != afterDecimalPointLength) {
			Util.addExceptionToReport("APR ratio length after decimal point mismatch", afterDecimalPointLength + "",
					expectedLength + "");
		}
	}

	/**
	 * Verifies the ratios values
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyRatios(String values) throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("************************ verifyRatios ******************************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		String actualRatioValue = getElementByUsing(args[0]).getText().trim();
		String expectedRatioValue = "";
		String proposedExpense = getElementByUsing(args[1]).getAttribute(args[4]).replace(",", "").trim();
		String totalMOIncome = getElementByUsing(args[2]).getAttribute(args[4]).replace(",", "").trim();
		String totalMOLiabilities = getElementByUsing(args[3]).getAttribute(args[4]).replace(",", "").trim();
		float first_housingRatio = (Float.parseFloat(proposedExpense) / Float.parseFloat(totalMOIncome)) * 100;
		float second_DTIRatio = ((Float.parseFloat(proposedExpense) + Float.parseFloat(totalMOLiabilities))
				/ Float.parseFloat(totalMOIncome)) * 100;
		BigDecimal calculatedFirstRatio = new BigDecimal(first_housingRatio);
		calculatedFirstRatio = calculatedFirstRatio.setScale(2, BigDecimal.ROUND_UP);

		BigDecimal calculatedSecondRatio = new BigDecimal(second_DTIRatio);
		calculatedSecondRatio = calculatedSecondRatio.setScale(2, BigDecimal.ROUND_UP);
		expectedRatioValue = calculatedFirstRatio + "0%/" + calculatedSecondRatio + "0%";
		System.out
				.println("expectedRatioValue : actualRatioValue ==> " + expectedRatioValue + " : " + actualRatioValue);
		if (!expectedRatioValue.equalsIgnoreCase(actualRatioValue)) {
			Util.addExceptionToReport("Ratios values mismatch", actualRatioValue, expectedRatioValue);
		}
	}

	/**
	 * Verifies the highlighted tabs
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyHighlighted(String values) throws Exception {
		String[] args = KWVariables.getVariables().get(values).split(":");
		List<WebElement> childs = getElementByUsing(args[0]).findElements(By.xpath(args[1]));

		if (args[2].equalsIgnoreCase(args[3])
				&& !childs.get(0).getAttribute(args[4]).replace("  ", " ").trim().equalsIgnoreCase(args[5])) {
			Util.addExceptionToReport("CURRENT TAB IS NOT HIGHLIGHTED", "HIGHLIGHTED BLUE", "NOT HIGHLIGHTED");
		} else if (!args[2].equalsIgnoreCase(args[3])
				&& childs.get(0).getAttribute(args[4]).replace("  ", " ").trim().equalsIgnoreCase(args[5])) {
			Util.addExceptionToReport("CURRENT TAB IS HIGHLIGHTED", "HIGHLIGHTED BLUE", "NOT HIGHLIGHTED");
		}
	}

	public void verifyandSelectRadio(String values) throws Exception {
		if (values != null) {
			String[] args = KWVariables.getVariables().get(values).split(":");
			if (args[1].equalsIgnoreCase("verify")) {
				List<WebElement> radioType = getElementByUsing(args[0]).findElements(By.xpath(args[2]));
				for (WebElement e : radioType) {
					if (e.isSelected()) {
						System.out.println("Selected Radio Button = " + e.getAttribute("id"));
					}
				}
			} else {
				List<WebElement> radioType = getElementByUsing(args[0]).findElements(By.xpath(args[2]));
				for (WebElement e : radioType) {
					if (e.getText().equals(args[1])) {
						e.click();
						System.out.println(args[0] + " Radio Button = " + e.getText());
					}
				}

			}
		}
	}

	public void verifySelectedOption(String values) throws Exception, IOException, TwfException {
		Select dropdown = new Select(getElementByUsing(values));
		System.out.println("Selected Value= " + dropdown.getFirstSelectedOption().getText());
	}

	/**
	 * Keyboard: Performs press tab action
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void pressTabKey(String values) throws Exception {
		if (values != null) {
			getElementByUsing(values).sendKeys(Keys.TAB);
			Thread.sleep(4000);
		}

	}

	/**
	 * Selects the dropdown option
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void selectdropdownValue(String values) throws Exception {
		String[] args = values.split(":");
		Select dropdown = new Select(getElementByUsing(args[0]));
		for (int i = 0; i < dropdown.getOptions().size(); i++) {
			if (args[1].equalsIgnoreCase(dropdown.getOptions().get(i).getText())) {
				dropdown.getOptions().get(i).click();
			}

		}
	}

	/**
	 * Selects the refi check box
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void selectRefiPurpose(String values) throws Exception {
		String[] args = KWVariables.getVariables().get(values).split(":");
		List<WebElement> checkboxtype = getElementByUsing(args[0]).findElements(By.xpath(args[2]));
		for (WebElement e : checkboxtype) {
			if (e.getAttribute("value").equalsIgnoreCase(args[1])) {
				e.click();
			}
		}
	}

	/**
	 * Verifies the tooltip
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifytoolTipOccurance(String values) throws Exception {
		String[] args = values.split(":");
		WebDriver driver = DriverFactory.getDriver();
		if (!waitTillElementToDisappear(args[1], elementDisplayTimeOut)) {
			Actions mousehover = new Actions(driver);
			mousehover.moveToElement(getElementByUsing(args[0])).perform();
			if (!getElementByUsing(args[1]).isDisplayed()) {
				Util.addExceptionToReport("Tool Tip not displayed on Hover" + getElementByUsing(args[0]).getText(),
						"Expected Tooltip", "Tool Tip missing");
			}
		}
	}

	/**
	 * Verifies the general alert
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifygeneralAlert(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		getElementByUsing(args[0]).click();
		String actualAlertText;
		String expectedAlertText = args[1].trim();
		Thread.sleep(1000);
		Alert alert = driver.switchTo().alert();
		actualAlertText = alert.getText().trim();
		switch (args[2]) {
		case "accept":
			alert.accept();
			break;
		case "dismiss":
			alert.dismiss();
			break;
		default:
			alert.dismiss();
		}

		if (!actualAlertText.equalsIgnoreCase(expectedAlertText)) {
			Util.addExceptionToReport("Alert text mismatch", actualAlertText, expectedAlertText);
		}
	}

	/**
	 * Verifies the attribute value
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyElementAttribute(String values) throws Exception {
		String[] args = KWVariables.getVariables().get(values).split(":");
		if (!getElementByUsing(args[0]).getAttribute(args[1]).equalsIgnoreCase(args[2])) {
			Util.addExceptionToReport("Text Mismatch", getElementByUsing(args[0]).getAttribute(args[1]), args[2]);
		}
	}

	/**
	 * Clicks on Cancel button
	 * 
	 * @param val
	 * @throws Exception
	 */
	public void dismissAlerts(String val) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		getElementByUsing(val).click();
		Thread.sleep(1000);
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	/**
	 * Verifies the archive alert text
	 * 
	 * @param expectedAlertText
	 * @throws Exception
	 */
	public void verifyArchiveAlert(String expectedAlertText) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		Thread.sleep(1000);
		Alert alert = driver.switchTo().alert();
		String actualAlertText = alert.getText().trim();
		if (!actualAlertText.contains(expectedAlertText)) {
			Util.addExceptionToReport("Alert text mismatch", actualAlertText, expectedAlertText);
		}
		alert.accept();
	}

	/**
	 * verifies the loan overview section changed values
	 * 
	 * @param values
	 * @throws Exception
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void verifyLoanOverviewChanges(String values) throws Exception, InvalidFormatException, IOException {
		String[] args = KWVariables.getVariables().get(values).split(":");
		float expected_ltv;
		String expected_ltvString = "";
		String purposeOfLoan = new Select(getElementByUsing(args[17])).getFirstSelectedOption().getText().toString()
				.trim();
		DecimalFormat df = new DecimalFormat(".000");
		float new_app_salesprice = Float.parseFloat(args[25]);
		float new_app_estimatedValue = Float.parseFloat(args[26]);
		String downpaymentpercentage = args[27];
		String downpaymnetdollars = args[28];
		String subordinatefinancing = args[29];
		String loanprogramno = args[30];
		String loanprogram = args[31];
		String new_property_address = args[32];
		String new_interestrate = args[33];
		String new_estddate = args[34];
		String new_income = args[35];
		String new_liabilities = args[36];
		getElementByUsing(args[19]).clear();
		getElementByUsing(args[19]).sendKeys(String.valueOf(new_app_salesprice));
		getElementByUsing(args[19]).sendKeys(Keys.TAB);
		getElementByUsing(args[3]).clear();
		getElementByUsing(args[3]).sendKeys(String.valueOf(new_app_estimatedValue));
		getElementByUsing(args[3]).sendKeys(Keys.TAB);
		getElementByUsing(args[4]).sendKeys(downpaymentpercentage);
		getElementByUsing(args[4]).sendKeys(Keys.TAB);
		Thread.sleep(4000);
		float new_app_loanamount = Math.round(Float.parseFloat(
				getElementByUsing(args[6]).getAttribute("value").replace("$", "").replaceAll(",", "").trim()));
		getElementByUsing(args[20]).click(); // Save the APP
		Thread.sleep(4000);
		float new_overview_loanamount = Float
				.parseFloat(getElementByUsing(args[1]).getText().replace("$", "").replaceAll(",", "").trim());
		float new_overview_estimatedValue = Float
				.parseFloat(getElementByUsing(args[2]).getText().replace("$", "").replaceAll(",", "").trim());

		if (!(new_app_estimatedValue == new_overview_estimatedValue)) {
			Util.addExceptionToReport("Mismatch in Estimated Value Verification",
					String.valueOf(new_overview_estimatedValue), String.valueOf(new_app_estimatedValue));
		}
		if (!(new_overview_loanamount == new_app_loanamount)) {
			Util.addExceptionToReport("Mismatch in Loan Amount Verification", String.valueOf(new_overview_loanamount),
					String.valueOf(new_app_loanamount));
		}

		String new_actual_ltvStr = getElementByUsing(args[0]).getText().split("%/")[0] + "%";
		if (!purposeOfLoan.equalsIgnoreCase("Purchase")) {
			expected_ltv = (new_app_loanamount / new_app_estimatedValue) * 100;
			expected_ltvString = expected_ltv + "";
			expected_ltvString = df.format(expected_ltv) + "%";
		} else {
			expected_ltv = (new_app_loanamount / Math.min(new_app_estimatedValue, new_app_salesprice)) * 100;
			expected_ltvString = expected_ltv + "";
			expected_ltvString = df.format(expected_ltv) + "%";
		}
		if (!(new_actual_ltvStr.equals(expected_ltvString))) {
			Util.addExceptionToReport("Mismatch in LTV Verification", String.valueOf(new_actual_ltvStr),
					String.valueOf(expected_ltvString));
		}
		// Verify Change in CLTV and DownPayment Dollars reflect in Loan
		// Overview header
		// Verification of Loan Program and Property Address
		getElementByUsing(args[3]).sendKeys(Keys.TAB);
		getElementByUsing(args[4]).sendKeys(Keys.TAB);
		getElementByUsing(args[5]).sendKeys(downpaymnetdollars);
		getElementByUsing(args[5]).sendKeys(Keys.TAB);
		Thread.sleep(2000);
		new_app_loanamount = Math.round(Float.parseFloat(
				getElementByUsing(args[6]).getAttribute("value").replace("$", "").replaceAll(",", "").trim()));
		if (!(new_app_loanamount == (Math.min(new_app_estimatedValue, new_app_salesprice))
				- Float.parseFloat(downpaymnetdollars))) {
			Util.addExceptionToReport("Mismatch in Base Loan Amount Verification", String.valueOf(new_app_loanamount),
					String.valueOf((new_app_estimatedValue) - Float.parseFloat(downpaymnetdollars)));
		}

		getElementByUsing(args[7]).clear();
		getElementByUsing(args[7]).sendKeys(subordinatefinancing);
		String expected_cltvstr = "";
		float expected_cltv = (float) Math.ceil(((Float.parseFloat(subordinatefinancing) + new_app_loanamount)
				/ Math.min(new_app_estimatedValue, new_app_salesprice)) * 100);
		expected_cltvstr = expected_cltv + "";
		expected_cltvstr = df.format(expected_cltv) + "%";

		// Property Address Verification
		getElementByUsing(args[21]).clear();
		getElementByUsing(args[21]).sendKeys(new_property_address);
		// Interest Rate Verification
		getElementByUsing(args[22]).clear();
		getElementByUsing(args[22]).sendKeys(new_interestrate);

		// MO Income Liabilities and Assets
		getElementByUsing(args[23]).clear();
		getElementByUsing(args[23]).sendKeys(new_income);
		getElementByUsing(args[23]).sendKeys(Keys.TAB);
		Thread.sleep(2000);
		getElementByUsing(args[24]).clear();
		getElementByUsing(args[24]).sendKeys(new_liabilities);

		getElementByUsing(args[15]).clear();
		getElementByUsing(args[15]).sendKeys(new_estddate);

		getElementByUsing(args[20]).click(); // Save the APP
		Thread.sleep(5000);

		String actual_cltv = getElementByUsing(args[0]).getText().split("%/")[1];
		String new_overview_income = getElementByUsing(args[12]).getText().replace("$", "").replaceAll(",", "").trim();
		String new_overview_liabilities = getElementByUsing(args[14]).getText().replace("$", "").replaceAll(",", "")
				.trim();

		if (!getElementByUsing(args[11]).getText().contains(new_interestrate)) {
			System.out.println("*********1**************");
			Util.addExceptionToReport("Mismatch in Interest Rate Verification", getElementByUsing(args[11]).getText(),
					new_interestrate);
		}
		if (!getElementByUsing(args[13]).getText().contains(new_property_address)) {
			System.out.println("*********2**************");
			Util.addExceptionToReport("Mismatch in Property Address Verification",
					getElementByUsing(args[13]).getText(), new_property_address);
		}
		if (!actual_cltv.equals(expected_cltvstr)) {
			System.out.println("*********3**************");
			Util.addExceptionToReport("Mismatch in CLTV Verification", actual_cltv, expected_cltvstr);
		}
		if (!getElementByUsing(args[16]).getText().equals(new_estddate)) {
			System.out.println("*********4**************");
			Util.addExceptionToReport("Mismatch in Estd Closing Date Verification",
					getElementByUsing(args[16]).getText(), new_estddate);
		}
		if (!new_overview_income.equals(new_income)) {
			System.out.println("*********5**************");
			Thread.sleep(60000);
			// Util.addExceptionToReport("Mismatch in Income Verification",
			// new_overview_income, new_income);
		}
		if (!new_overview_liabilities.equals(new_liabilities)) {
			Thread.sleep(60000);
			System.out.println("*********6**************");
			// Util.addExceptionToReport("Mismatch in Income Verification",
			// new_overview_liabilities, new_liabilities);
		}

		// Loan Program Verification
		getElementByUsing(args[9]).sendKeys(loanprogramno);
		getElementByUsing(args[9]).sendKeys(Keys.TAB);
		Thread.sleep(5000);
		if (!new Select(getElementByUsing(args[8])).getFirstSelectedOption().getText().equals(loanprogram)) {
			System.out.println("*********7**************");
			Util.addExceptionToReport("Mismatch in Loan Program",
					new Select(getElementByUsing(args[8])).getFirstSelectedOption().getText(), loanprogram);
		}

		getElementByUsing(args[20]).click(); // Save the APP
		Thread.sleep(5000);

		if (!getElementByUsing(args[10]).getText()
				.equalsIgnoreCase(new Select(getElementByUsing(args[8])).getFirstSelectedOption().getText())) {
			System.out.println("*********8**************");
			Util.addExceptionToReport("Mismatch in Loan Program Overview Header Verification",
					getElementByUsing(args[10]).getText(),
					new Select(getElementByUsing(args[8])).getFirstSelectedOption().getText());
		}
	}

	/**
	 * Verifies the default sorting option
	 * 
	 * @param values
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyDefaultLoansSorting(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String classAtributeValue = getElementByUsing(args[0]).getAttribute(args[1]);
		if (!classAtributeValue.trim().equalsIgnoreCase(args[2])) {
			Util.addExceptionToReport("Default sorting is Mismatch", classAtributeValue, args[2]);
		}
	}

	static List<String> loanid = new ArrayList<>();

	/**
	 * Stores loan id for future usage
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void saveLoanid(String values) throws Exception, IOException, TwfException {
		String mykeystatus = getElementByUsing(values).getText();
		String applicationId = mykeystatus.substring(mykeystatus.indexOf("ID") + 3, mykeystatus.length());
		loanid.add(applicationId);
	}

	/**
	 * Compares the loan id's
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void compareLoanids(String values) throws Exception {
		if (loanid.get(0).equals(loanid.get(1))) {
			Util.addExceptionToReport("Application Id is same as before", loanid.get(1), loanid.get(0));
		}
	}

	public void verifyErrorPushtoMykey(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(",");

		// String
		// ar[]=("https://mycircle-qa-08.guildmortgage.com/login?myapps/data-push/test.php,AppID_txb,1003Test_rtb,Submit_txb,Successresponse,99999".split(","));
		String url = args[0].replace("login?", "");

		System.out.println("url : " + url);
		// driver.navigate().to("https://mycircle-qa-08.guildmortgage.com/myapps/data-push/test.php
		// ");
		driver.navigate().to(url);
		Util.switchToWindow(driver);
		String errormessage = "Error trying to upload quickapp_id";

		// Check for No QuickApp
		/*
		 * waitForElement(getElementByUsing("AppID_txb"), 120);
		 * getElementByUsing("AppID_txb").sendKeys(" "); Thread.sleep(2000);
		 * getElementByUsing("1003Test_rtb").click(); Thread.sleep(2000);
		 * getElementByUsing("Submit_txb").click();
		 */

		waitForElement(getElementByUsing(args[1]), 120);
		getElementByUsing(args[1]).sendKeys(" ");
		Thread.sleep(2000);
		getElementByUsing(args[2]).click();
		Thread.sleep(2000);
		getElementByUsing(args[3]).click();
		Thread.sleep(20000);
		System.out.println(getElementByUsing(args[4]).getText());
		if (!getElementByUsing(args[4]).getText().contains(errormessage)) {
			Util.addExceptionToReport("No error message displayed", getElementByUsing(args[4]).getText(), errormessage);
		}

		// Check for WrongQuickApp
		waitForElement(getElementByUsing(args[1]), 120);
		getElementByUsing(args[1]).sendKeys(args[5]);
		Thread.sleep(2000);
		getElementByUsing(args[2]).click();
		Thread.sleep(2000);
		getElementByUsing(args[3]).click();
		Thread.sleep(20000);
		System.out.println(getElementByUsing(args[4]).getText());
		if (!getElementByUsing(args[4]).getText().contains(errormessage)) {
			Util.addExceptionToReport("No error message displayed", getElementByUsing(args[4]).getText(), errormessage);
		}
		Thread.sleep(20000);
	}

	/**
	 * Verifies the default fee
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyDefaultFees(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(":");
		List<WebElement> allFees = driver.findElements(By.xpath(args[0]));
		List<WebElement> blankFees = new ArrayList<>();
		for (int i = 0; i < allFees.size(); i++) {
			if (allFees.get(i).getText().contains("$") || allFees.get(i).getText().contains("%")) {
				blankFees.add(allFees.get(i));
			}
		}
		allFees.retainAll(blankFees);
		blankFees.clear();
		// To delete multiple entry for Recurring
		if (args[2].equalsIgnoreCase("Non-Recurring"))
			allFees.remove(1);

		List<WebElement> alldefaultFees = driver.findElements(By.xpath(args[1]));
		for (int i = 0; i < alldefaultFees.size(); i++) {
			if (alldefaultFees.get(i).getText().contains("$") || alldefaultFees.get(i).getText().contains("%")) {
				blankFees.add(alldefaultFees.get(i));
			}
		}
		alldefaultFees.retainAll(blankFees);

		for (int i = 0; i < allFees.size(); i++) {
			if (!alldefaultFees.get(i).getText().contains(allFees.get(i).getText())) {
				Util.addExceptionToReport("DefaultFees is Wrongly displayed", alldefaultFees.get(i).getText(),
						allFees.get(i).getText());
			}
		}
	}

	/**
	 * Enter the fee values
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void inputFeevalues(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(":");
		List<WebElement> feevalues = driver.findElements(By.xpath(args[0]));
		if (args[2].equalsIgnoreCase("Non-Recurring")) {
			feevalues.remove(0);
			feevalues.remove(1);
		}
		if (args[2].equalsIgnoreCase("Non-RecurringMisc")) {
			feevalues = feevalues.subList(0, 10);
		}

		if (args[2].equalsIgnoreCase("RealEstate")) {
			feevalues = feevalues.subList(0, 4);
		}
		for (WebElement e : feevalues) {
			if (!e.getAttribute("class").contains("hide")) {
				e.clear();
				e.sendKeys(args[1]);
			}
		}
		feevalues.clear();
	}

	/**
	 * Select the BPO values
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void selectPBOvalues(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(":");
		List<WebElement> dropdowns = driver.findElements(By.xpath(args[0]));
		for (WebElement e : dropdowns) {
			Select dd = new Select(e);
			for (int i = 0; i < dd.getOptions().size(); i++) {
				if (args[1].equalsIgnoreCase(dd.getOptions().get(i).getText())) {
					dd.getOptions().get(i).click();
				}
			}
		}
		dropdowns.clear();
	}

	/**
	 * Verifies the total row value
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyTotalRowFees(String values) throws Exception {
		System.out.println("**************** verifyTotalRowFees ***********************");
		WebDriver driver = DriverFactory.getDriver();
		String args = KWVariables.getVariables().get(values);
		// Buyer Fees
		Float buyerFees = Float.parseFloat((driver.findElement(By.id(args + "_buyer")).getAttribute("value"))
				.replace("$", "").replaceAll(",", "").trim());
		Float sellerFees = Float.parseFloat((driver.findElement(By.id(args + "_seller")).getAttribute("value"))
				.replace("$", "").replaceAll(",", "").trim());
		Float lenderFees = Float.parseFloat((driver.findElement(By.id(args + "_lender")).getAttribute("value"))
				.replace("$", "").replaceAll(",", "").trim());
		Float pboFees = Float.parseFloat((driver.findElement(By.id(args + "_pbo")).getAttribute("value"))
				.replace("$", "").replaceAll(",", "").trim());
		Float totalFees = Float.parseFloat(
				(driver.findElement(By.id(args + "_total")).getText()).replace("$", "").replaceAll(",", "").trim());

		if (!((buyerFees + sellerFees + lenderFees + pboFees) == totalFees)) {
			Util.addExceptionToReport("Total Fees is Wrongly displayed", String.valueOf(totalFees),
					String.valueOf(buyerFees + sellerFees + lenderFees + pboFees));
		}

		// Verify Change in buyer fees changes sum
		driver.findElement(By.id(args + "_buyer")).clear();
		driver.findElement(By.id(args + "_buyer")).sendKeys("100");
		driver.findElement(By.id(args + "_buyer")).sendKeys(Keys.TAB);
		Thread.sleep(1000);
		totalFees = Float.parseFloat(
				(driver.findElement(By.id(args + "_total")).getText()).replace("$", "").replaceAll(",", "").trim());
		if (!((100.00 + sellerFees + lenderFees + pboFees) == totalFees)) {
			Util.addExceptionToReport("Total Fees1 is Wrongly displayed", String.valueOf(totalFees),
					String.valueOf(buyerFees + sellerFees + lenderFees));
		}
		buyerFees = Float.parseFloat((driver.findElement(By.id(args + "_buyer")).getAttribute("value")).replace("$", "")
				.replaceAll(",", "").trim());
		// Verify Change in lender fees changes sum
		driver.findElement(By.id(args + "_lender")).clear();
		driver.findElement(By.id(args + "_lender")).sendKeys("100");
		driver.findElement(By.id(args + "_lender")).sendKeys(Keys.TAB);
		Thread.sleep(1000);
		totalFees = Float.parseFloat(
				(driver.findElement(By.id(args + "_total")).getText()).replace("$", "").replaceAll(",", "").trim());
		if (!((100.00 + buyerFees + sellerFees + pboFees) == totalFees)) {
			Util.addExceptionToReport("Total Fees2 is Wrongly displayed", String.valueOf(totalFees),
					String.valueOf(buyerFees + sellerFees + lenderFees));
		}
		System.out.println("************ verifyTotalRowFees Done !!!!!!!!!!!!!!!! ********************");
	}

	/**
	 * Verifies the PDF src value
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyPDFsrc(String values) throws Exception, IOException, TwfException {
		String[] args = values.split(":");
		if (!getElementByUsing(args[0]).getAttribute("src").contains(args[1])) {
			Util.addExceptionToReport("PDF Format doesn't change", getElementByUsing(args[0]).getAttribute("src"),
					args[1]);
		}
	}

	/**
	 * Verifies the downloaded file
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyFileDownloaded(String values) throws Exception {
		boolean filefound = false;
		Thread.sleep(1000);
		String[] args = KWVariables.getVariables().get(values).split(",");
		String filePath = System.getProperty("user.home") + File.separator + args[0] + File.separator;
		File dir = new File(filePath);
		File[] dirContents = dir.listFiles();
		for (int i = 0; i < dirContents.length; i++) {

			if (dirContents[i].getName().equals(args[1])) {
				filefound = true;
				// Date unixDate = new Date(dirContents[i].lastModified());
				// Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
			}
		}
		if (!filefound)
			Util.addExceptionToReport("File is not downloaded on target location",
					"Finding downloaded file in = " + args[0], args[1] + " Should be present");
	}

	/**
	 * Verifies the mail inbox
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyemailInbox(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(",");
		driver.navigate().to(args[0]);
		Util.switchToWindow(driver);
		driver.findElement(By.id("login")).sendKeys(args[1]);
		driver.findElement(By.xpath("//*[@class=\"sbut\"]")).click();
		Thread.sleep(2000);
		WebElement mailframe = driver.findElement(By.id("ifmail"));
		driver.switchTo().frame(mailframe);
		String mailheader = driver.findElement(By.id("mailhaut")).getText();
		if (!(mailheader.contains(args[2]) || mailheader.contains(args[3]) || mailheader.contains(args[4]))) {
			Util.addExceptionToReport("Email Not Recieved Yet", "Email missing in " + args[0],
					"Email should be recieved in " + args[0]);
		}
		driver.navigate().back();
	}

	/**
	 * Verifies the ESP prepaid
	 * 
	 * @param values
	 * @throws Exception
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void verifyESTPrepaid(String values) throws Exception, InvalidFormatException, IOException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		Float loanamount = Float
				.parseFloat(getElementByUsing(args[0]).getText().replace("$", "").replaceAll(",", "").trim());
		Float interest = Float
				.parseFloat(getElementByUsing(args[1]).getText().replace("%", "").replaceAll(",", "").trim());
		Float nodays = Float.parseFloat(getElementByUsing(args[2]).getAttribute("value"));
		Float interestAmount = Float.parseFloat(
				getElementByUsing(args[3]).getAttribute("value").replace("$", "").replaceAll(",", "").trim());
		Float perdayIntt = (float) ((loanamount * interest) / 100.00) / 365;
		// Need Exact Formula of Calculating EST Interest Rate
		if (!(Math.ceil(perdayIntt * nodays) == Math.ceil(interestAmount))) {
			Util.addExceptionToReport("EST Prepaid Interest Value is wrong",
					String.valueOf(((loanamount * interest) / 100.00) * (nodays / 365)),
					String.valueOf(interestAmount));
		}
		// Change in No. of days
		getElementByUsing(args[2]).clear();
		getElementByUsing(args[2]).sendKeys("25");
		getElementByUsing(args[2]).sendKeys(Keys.TAB);
		interestAmount = Float.parseFloat(
				getElementByUsing(args[3]).getAttribute("value").replace("$", "").replaceAll(",", "").trim());
		if (!(Math.ceil(perdayIntt * 25.0) == Math.ceil(interestAmount))) {
			Util.addExceptionToReport("EST1 Prepaid Interest Value is wrong",
					String.valueOf(((loanamount * interest) / 100.00) * (25 / 365)), String.valueOf(interestAmount));
		}
	}

	/**
	 * Verifies the cash to close total value
	 * 
	 * @param values
	 * @throws Exception
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void verifyCashtoCloseTotal(String values) throws Exception, InvalidFormatException, IOException {
		String args[] = KWVariables.getVariables().get(values).split(":");
		// verify Down Payment Update
		Float downpayment = Float
				.parseFloat(getElementByUsing(args[5]).getText().replace("$", "").replaceAll(",", "").trim());
		Float init_downpayment = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[4])
				.replace("$", "").replaceAll(",", "").trim());
		Float fin_repair = Float.parseFloat(
				getElementByUsing(args[0]).getAttribute("value").replace("$", "").replaceAll(",", "").trim());
		Float fin_land = Float.parseFloat(
				getElementByUsing(args[1]).getAttribute("value").replace("$", "").replaceAll(",", "").trim());
		if (!((init_downpayment + fin_repair + fin_land) == downpayment)) {
			Util.addExceptionToReport("Downpayment Value is wrong", String.valueOf(downpayment),
					String.valueOf(init_downpayment + fin_repair + fin_land));
		}
		// Verify Sub Total update
		Float init_subtotal = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[6])
				.replace("$", "").replaceAll(",", "").trim());
		Float subtotal = Float
				.parseFloat(getElementByUsing(args[8]).getText().replace("$", "").replaceAll(",", "").trim());
		if (!((init_subtotal + fin_repair + fin_land) == subtotal)) {
			Util.addExceptionToReport("Subtotal Value is wrong", String.valueOf(subtotal),
					String.valueOf(init_subtotal + fin_repair + fin_land));
		}
		// Verify Cash to Close Amount
		Float init_cashtotal = Float.parseFloat(MyTestCaseExecuter.stepobject.getKwValueVariables().get(args[7])
				.replace("$", "").replaceAll(",", "").trim());
		Float cashtotal = Float
				.parseFloat(getElementByUsing(args[9]).getText().replace("$", "").replaceAll(",", "").trim());
		Float lender_rebate = Float.parseFloat(
				getElementByUsing(args[2]).getAttribute("value").replace("$", "").replaceAll(",", "").trim());
		Float seller_credit = Float.parseFloat(
				getElementByUsing(args[3]).getAttribute("value").replace("$", "").replaceAll(",", "").trim());
		if (!((init_cashtotal + fin_repair + fin_land - lender_rebate - seller_credit) == cashtotal)) {
			Util.addExceptionToReport("Cash to Close Total Value is wrong", String.valueOf(cashtotal),
					String.valueOf(init_cashtotal + fin_repair + fin_land - seller_credit - seller_credit));
		}
	}

	/**
	 * Verifies the mis default value
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyMiscDefaultFees(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String[] args = KWVariables.getVariables().get(values).split(":");
		List<WebElement> allFees = driver.findElements(By.xpath(args[0]));
		List<WebElement> alldefaultFees = driver.findElements(By.xpath(args[1]));
		List<WebElement> blankFees = new ArrayList<>();
		List<String> defaultfees = new ArrayList<>();
		for (int i = 0; i < alldefaultFees.size(); i++) {
			if (alldefaultFees.get(i).getText().contains("$")) {
				blankFees.add(alldefaultFees.get(i));
			}
		}
		if (!(blankFees.size() == 0)) {
			alldefaultFees.retainAll(blankFees);
			for (int i = 0; i < alldefaultFees.size(); i++) {
				if (allFees.get(i).getAttribute("value") == null || allFees.get(i).getAttribute("value").equals("")) {
					defaultfees.add("$0.00");
				} else {
					defaultfees.add(allFees.get(i).getAttribute("value"));
				}
			}
			for (int i = 0; i < alldefaultFees.size(); i++) {
				if (!(alldefaultFees.get(i).getText().contains(defaultfees.get(i)))) {
					Util.addExceptionToReport("Default Fees is Wrongly displayed", alldefaultFees.get(i).getText(),
							defaultfees.get(i));
				}
			}
		} else {
			if (!alldefaultFees.get(0).getText().contains("No Default fees")) {
				Util.addExceptionToReport("No Default Fees Text is not diplayed", alldefaultFees.get(0).getText(),
						("No Default fees"));
			}
		}

	}

	/**
	 * Verifies the credit reporting issue date
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void verifyCreditReportissueDate(String values) throws Exception, IOException, TwfException {
		String timestamp = getElementByUsing(values).getText();
		System.out.println(timestamp);
		Date date = new Date();
		String DATE_FORMAT = "MM/dd/yyyy";
		DateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
		if (!timestamp.contains(sdf.format(date))) {
			Util.addExceptionToReport("Credit Report Issued date is wrong ", sdf.format(date), timestamp);
		}
	}

	/**
	 * Switch back to main page
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void switchBacktomain(String values) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String originalHandle = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				driver.switchTo().window(handle);
				driver.close();
			}
		}
		driver.switchTo().window(originalHandle);
	}

	/**
	 * Scrolls to the web element
	 * 
	 * @param ele
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void scrollToElement(String ele) throws Exception, IOException, TwfException {
		WebDriver driver = DriverFactory.getDriver();
		WebElement ele2 = getElementByUsing(ele);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(4000,0)", ele2);
		js.executeScript("window.scrollBy(4000,0)", ele2);
		js.executeScript("arguments[0].scrollIntoView(true);", ele2);
	}

	/**
	 * @author rahul.kunjumon, This method is to verify if the ShowAll and
	 *         HideAll button in 1003 page is working fine
	 * @param values
	 * @throws Exception
	 */
	public void verifyShowallHideall(String values) throws Exception {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String val[] = args[0].split(",");
		String val1 = args[1];
		for (int i = 0; i < val.length; i++) {
			String attributeHide = getElementByUsing(val[i]).getAttribute("class");
			scrollToElement(val[i]);
			// scrollT
			if (!attributeHide.equals(val1)) {
				Util.addExceptionToReport("Test Failure", "expected-" + val1, "Actual-" + attributeHide);
			}

		}
	}

	/**
	 * This method is to verify the Borrower information in the 1003 page
	 * 
	 * @author rahul.kunjumon,
	 * @param values
	 * @throws Exception
	 */
	public void verifyTenOThreeBorInfo(String values) throws Exception {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String val[] = args[0].split(",");
		String val1[] = args[1].split(",");
		for (int i = 0; i < val.length; i++) {
			String borrdetailsMA = MyTestCaseExecuter.stepobject.getKwValueVariables().get(val1[i]);
			String borrdetailsTenOThree = getElementByUsing(val[i]).getAttribute(args[4]);
			if (borrdetailsMA.equalsIgnoreCase(args[2]) || borrdetailsMA.equalsIgnoreCase(args[3])) {
				if (borrdetailsMA.equalsIgnoreCase(args[3])) {
					if (borrdetailsTenOThree != null) {
						Util.addExceptionToReport("Borrower Details mismatch", borrdetailsMA + "",
								borrdetailsTenOThree + "");
					}
				}
				if (borrdetailsMA.equalsIgnoreCase(args[2])) {
					String dob = "";
					if (!dob.equals(borrdetailsTenOThree)) {
						Util.addExceptionToReport("Borrower Details mismatch", borrdetailsMA + "",
								borrdetailsTenOThree + "");
					}
				}
			} else {
				if (i <= 4) {
					if (!borrdetailsMA.equals(borrdetailsTenOThree)) {
						Util.addExceptionToReport("Borrower Details mismatch", borrdetailsMA + "",
								borrdetailsTenOThree + "");
					}
				} else {
					String borrdetailsFICO = getElementByUsing(val[i]).getText();
					if (!borrdetailsMA.equals(borrdetailsFICO)) {
						Util.addExceptionToReport("Borrower Details mismatch", borrdetailsMA + "",
								borrdetailsFICO + "");
					}
				}
			}
		}
	}

	/**
	 * @author rahul.kunjumon, This method is to update the Income information
	 *         in the 1003 page and verify it
	 * @param values
	 * @throws Exception
	 */
	public void verifyUpdateIncome(String values) throws Exception {
		String args[] = KWVariables.getVariables().get(values).split(":");
		String val[] = args[0].split(",");
		String val1[] = args[1].split(",");
		for (int i = 0; i < val.length; i++) {
			String enteredIncomeDetails = MyTestCaseExecuter.stepobject.getKwValueVariables().get(val1[i]);
			String savedIncomeDetails = getElementByUsing(val[i]).getAttribute(args[2]);
			if (savedIncomeDetails.contains("$")) {
				savedIncomeDetails = savedIncomeDetails.replaceAll(",", "");
				savedIncomeDetails = savedIncomeDetails.substring(1, savedIncomeDetails.length() - 3);

			} else if (savedIncomeDetails.contains(".")) {
				savedIncomeDetails = savedIncomeDetails.substring(0, (savedIncomeDetails.indexOf(".")));
			}

			if (enteredIncomeDetails.contains("$")) {
				enteredIncomeDetails = enteredIncomeDetails.replaceAll(",", "");
				enteredIncomeDetails = enteredIncomeDetails.substring(1, enteredIncomeDetails.length() - 3);

			} else if (enteredIncomeDetails.contains(".")) {
				enteredIncomeDetails = enteredIncomeDetails.substring(0, (enteredIncomeDetails.indexOf(".")));
			}
			if (!enteredIncomeDetails.equals(savedIncomeDetails)) {
				Util.addExceptionToReport("Income Details mismatch", savedIncomeDetails + "",
						enteredIncomeDetails + "");
			}
		}
	}

	/**
	 * @author rahul.kunjumon, this method is to handle any popups coming in the
	 *         application.
	 * @param val
	 * @return
	 * @throws Exception
	 */
	public String popupHandler(String val) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		String[] value = val.split(",");
		getElementByUsing(value[1]).click();
		Thread.sleep(4000);
		try {
			Alert alert = driver.switchTo().alert();

			Thread.sleep(2000);
			String alertMessage = alert.getText();
			System.out.println(alertMessage);
			if (value[0].equalsIgnoreCase("OK")) {
				alert.accept();
			}
			if (value[0].equalsIgnoreCase("CANCEL")) {
				alert.dismiss();
			}
			return (alertMessage);
		} catch (Exception e) {
			return "No alert found";
		}
	}

	/**
	 * @author rahul.kunjumon, This method is to verify the error messages in
	 *         Add Group without entering Any details
	 * @param dataPoolArgs
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyErrorsForAddGroup(Map<String, String> dataPoolArgs)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		driver = DriverFactory.getDriver();
		String elements[] = dataPoolArgs.get("RepoElements").split(",");

		String localDataColumns[] = dataPoolArgs.get("DataColumns").split(",");

		for (int i = 0; i < localDataColumns.length; i++) {
			String values[] = dataPoolArgs.get(localDataColumns[i]).split(":");
			getElementByUsing(elements[0]).sendKeys(values[2].trim());
			getElementByUsing(elements[1]).sendKeys(values[3].trim());
			getElementByUsing(elements[2]).sendKeys(values[4].trim());
			getElementByUsing(elements[3]).sendKeys(values[5].trim());
			Thread.sleep(1000);
			getElementByUsing(elements[4]).click();
			Thread.sleep(2000);
			String expectedErrorMessage = values[0];
			List<WebElement> errorsList = driver.findElements(By.xpath(values[1]));
			String actualErrorMsg = "";
			for (int j = 0; j < errorsList.size(); j++) {
				actualErrorMsg = actualErrorMsg + errorsList.get(j).getText().trim() + "\n";
			}
			actualErrorMsg = actualErrorMsg.trim();
			expectedErrorMessage = expectedErrorMessage.trim();

			if (!expectedErrorMessage.equalsIgnoreCase(actualErrorMsg)) {
				Util.addExceptionToReport("Warning info mismatch", actualErrorMsg, expectedErrorMessage);
			}
		}
	}

	/**
	 * @author rahul.kunjumon,An extension of popupHandler method, this method
	 *         compares the alert message with the given text
	 * @param val
	 * @throws Exception
	 */
	public void popupHandlerVerify(String val) throws Exception {
		String[] value = val.split(":");
		String ActualValue = popupHandler(value[0]);
		if (!value[1].equals(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}

	/**
	 * @author rahul.kunjumon, this method is get the selected option from the
	 *         dropdown and compare it with the value passed
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 *             value
	 */
	public void compareSelectedOption(String val) throws Exception, IOException, TwfException {
		String[] value = val.split(",");
		Select dropdown = new Select(getElementByUsing(value[0]));
		String ActualValue = dropdown.getFirstSelectedOption().getAttribute(value[2]);
		if (!value[1].equals(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}

	/**
	 * @author rahul.kunjumon, this method captures the selected option and
	 *         stores it in a static variable
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void getSelectedOption(String val) throws Exception, IOException, TwfException {
		Select dropdown = new Select(getElementByUsing(val));
		selectedOption = dropdown.getFirstSelectedOption().getAttribute("value");
	}

	String MyApp = "MyApp";
	String MyKey = "MyKey";

	/**
	 * @author rahul.kunjumon, this method is used to enter the AppID for MyApps
	 *         / Application ID of Mykey and thereby search for that Application
	 *         this method is also used to enter the state for MyApps / state
	 *         for Mykey and thereby search for all the application under the
	 *         entered state
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */

	public void compareAppIDState(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String[] value = KWVariables.getVariables().get(val).split(":");
		String[] reponame = value[0].split(",");
		String[] xpaths = value[1].split(",");
		String searchrepo = value[2];
		if (value[5].equalsIgnoreCase(MyApp)) {
			String[] inputvalue = new String[] { quickAppId, selectedOption };
			for (int i = 0; i < inputvalue.length; i++) {
				Thread.sleep(2000);
				getElementByUsing(reponame[i]).sendKeys(inputvalue[i]);
				Thread.sleep(2000);
				getElementByUsing(searchrepo).click();
				Thread.sleep(2000);
				List<WebElement> appIDColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(xpaths[i]));

				if (appIDColumnDataDisplayed.size() > 0) {
					for (int j = 0; j < appIDColumnDataDisplayed.size(); j++) {
						String actualValue = appIDColumnDataDisplayed.get(j).getText().trim();
						if (!actualValue.contains(inputvalue[i])) {
							Util.addExceptionToReport("Filtering of loans failed", actualValue, inputvalue[i]);
						}
					}
				}
				getElementByUsing(reponame[i]).clear();
			}

		} else if (value[5].equalsIgnoreCase(MyKey)) {
			String[] inputvalue = new String[] { applicationId, selectedOption };
			for (int i = 0; i < inputvalue.length; i++) {
				Thread.sleep(2000);
				if (i == 0) {
					getElementByUsing(value[3]).sendKeys(value[4]);
				}
				getElementByUsing(reponame[i]).sendKeys(inputvalue[i]);
				Thread.sleep(2000);
				getElementByUsing(searchrepo).click();
				Thread.sleep(2000);
				List<WebElement> appIDColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(xpaths[i]));

				if (appIDColumnDataDisplayed.size() > 0) {
					for (int j = 0; j < appIDColumnDataDisplayed.size(); j++) {
						String actualValue = appIDColumnDataDisplayed.get(j).getText().trim();
						if (!actualValue.contains(inputvalue[i])) {
							Util.addExceptionToReport("Filtering of loans failed", actualValue, inputvalue[i]);
						}
					}
				}
				getElementByUsing(reponame[i]).clear();
				getElementByUsing(value[3]).clear();
			}
		}
	}

	/**
	 * @author rahul.kunjumon,this method is used for both myApps and myKeys.It
	 *         verifies search results for various CopyApp search params
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void searchForCopyApp(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String[] value = KWVariables.getVariables().get(val).split(":");
		String[] reponame = value[0].split(",");
		String[] xpaths = value[1].split(",");
		String[] storedvalue = value[2].split(",");
		String searchrepo = value[3];

		for (int i = 0; i < xpaths.length; i++) {
			String StoredValueActual = MyTestCaseExecuter.stepobject.getKwValueVariables().get(storedvalue[i]);
			getElementByUsing(reponame[i]).sendKeys(StoredValueActual);
			Thread.sleep(2000);
			getElementByUsing(searchrepo).click();
			Thread.sleep(2000);
			List<WebElement> ColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(xpaths[i]));

			if (ColumnDataDisplayed.size() > 0) {
				StoredValueActual = StoredValueActual.toUpperCase();
				if (i != 2) {
					for (int i1 = 0; i1 < ColumnDataDisplayed.size(); i1++) {
						String actualValue = ColumnDataDisplayed.get(i1).getText().trim().toUpperCase();
						System.out.println("actualValue :" + actualValue);
						if (!actualValue.contains(StoredValueActual)) {
							Util.addExceptionToReport("Filtering of loans failed", actualValue, StoredValueActual);
						}
					}
				} else {

					for (int i1 = 0; i1 < ColumnDataDisplayed.size(); i1++) {
						String actualValue = ColumnDataDisplayed.get(i1).getText().trim().toUpperCase();
						System.out.println("actualValue :" + actualValue);
						if (!actualValue.contains(StoredValueActual)) {
							Util.addExceptionToReport("Filtering of loans failed", actualValue, StoredValueActual);
						}
					}
				}

			}
			getElementByUsing(reponame[i]).clear();
			Thread.sleep(2000);
		}
	}

	/**
	 * @author rahul.kunjumon, this method is to validate the errors in CopApp
	 *         page- MyKey
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void copyAppErrorValidation(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		driver = DriverFactory.getDriver();
		String[] value = KWVariables.getVariables().get(val).split(":");
		String[] reponame = value[0].split(",");
		String[] inputvalue = new String[] { value[2], applicationId };
		for (int i = 0; i < reponame.length; i++) {
			getElementByUsing(reponame[i]).sendKeys(inputvalue[i]);
			Thread.sleep(2000);
			getElementByUsing(value[1]).click();
			Thread.sleep(2000);
			String expectedErrorMessage = value[4];
			Thread.sleep(2000);
			String actualErrorMsg = driver.findElement(By.xpath(value[3])).getText();

			if (!expectedErrorMessage.equalsIgnoreCase(actualErrorMsg)) {
				Util.addExceptionToReport("Warning info mismatch", actualErrorMsg, expectedErrorMessage);
			}
			getElementByUsing(reponame[i]).clear();
		}
	}

	/**
	 * @author rahul.kunjumon, this methiod is used to verify zipcode as the
	 *         search field and search for the appid corresponding to the
	 *         zipcode from where it was captured
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void compareAppIDzip(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String[] value = KWVariables.getVariables().get(val).split(":");
		String reponamezip = value[0];
		String xpaths = value[1];
		String searchrepo = value[3];
		String inputvalue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(value[2]);
		Thread.sleep(2000);
		getElementByUsing(reponamezip).sendKeys(inputvalue);
		Thread.sleep(2000);
		getElementByUsing(searchrepo).click();
		Thread.sleep(2000);
		ArrayList<String> list = new ArrayList<String>();
		List<WebElement> appIDColumnDataDisplayed = DriverFactory.getDriver().findElements(By.xpath(xpaths));

		if (appIDColumnDataDisplayed.size() > 0) {
			for (int j = 0; j < appIDColumnDataDisplayed.size(); j++) {
				String actualValue = appIDColumnDataDisplayed.get(j).getText().trim();
				list.add(actualValue);
			}
		}
		if (value[4].equalsIgnoreCase(MyApp)) {
			if (!list.contains(quickAppId)) {
				Util.addExceptionToReport("Warning info mismatch", inputvalue, quickAppId);

			}
		} else if (value[4].equalsIgnoreCase(MyKey)) {
			if (!list.contains(applicationId)) {
				Util.addExceptionToReport("Warning info mismatch", inputvalue, applicationId);

			}
		}
	}

	/**
	 * @author rahul.kunjumon,this method is to verify the links of various
	 *         mortgage insurance companies and verify if they load properly
	 * @param values
	 * @throws Exception
	 */
	public void verifyMILink(String values) throws Exception {
		System.out.println("******************** verifyMILink *****************");
		String val[] = KWVariables.getVariables().get(values).split("::");
		String args[] = val[0].split(",");
		WebDriver driver = DriverFactory.getDriver();
		List<WebElement> linksList = driver.findElements(By.xpath(args[0]));
		String expectedMICompanyURL = "";
		String actualMICompanyURL = "";
		String actualMICompanyURLMismatch = "";
		String expectedMICompanyURLMismatch = "";
		boolean isMismatch = false;
		String winHandleBefore = driver.getWindowHandle();
		for (int i = 0; i < linksList.size(); i++) {

			String linkName = driver.findElement(By.xpath(val[1] + (i + 1) + val[2])).getText().trim();
			System.out.println("linkName :: " + linkName);

			// Genworth,MGIC,Radian LPMI Rate,National
			String slowlyLoadingSites = "Genworth,MGIC,Radian LPMI Rate,National,National Rate Finder | Master Policy # 00247";
			if (!slowlyLoadingSites.contains(linkName)) {
				// System.out.println("slowlyLoadingSites :
				// "+slowlyLoadingSites);
				// }

				// if(!linkName.equalsIgnoreCase("Genworth")){
				// if(!linkName.equalsIgnoreCase("MGIC")){
				driver.findElement(By.xpath(val[1] + (i + 1) + val[2])).click();

				// }
				// }
				System.out.println("Waiting for page to load ");

				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
					Thread.sleep(500);
					waitForPageToLoad("1000");
					System.out.println("Page title !!!!!!!!!!!!!!!" + driver.getTitle());
					Thread.sleep(500);
				}
				actualMICompanyURL = driver.getCurrentUrl();
				Thread.sleep(500);
				driver.close();
				driver.switchTo().window(winHandleBefore);

				expectedMICompanyURL = args[(i + 1)];
				if (!expectedMICompanyURL.equalsIgnoreCase(actualMICompanyURL)) {
					actualMICompanyURLMismatch = actualMICompanyURLMismatch + expectedMICompanyURL;
					expectedMICompanyURLMismatch = expectedMICompanyURLMismatch + expectedMICompanyURL;
					// }
				}
			}
		}

		if (isMismatch) {
			Util.addExceptionToReport("MI Companies Names mismatch:", actualMICompanyURLMismatch, expectedMICompanyURL);
		}
	}

	/**
	 * @author rahul.kunjumon, this method is to verify the drop down values in
	 *         the "Show" drop down field in MyAPps pipeline
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 */
	public void getAndCompareDropdown(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		String values[] = KWVariables.getVariables().get(val).split(":");
		String dropdownExpected[] = values[1].split(",");
		WebDriver driver = DriverFactory.getDriver();
		WebElement selectElement = driver.findElement(By.xpath(values[0]));
		Select select = new Select(selectElement);
		List<WebElement> allOptions = select.getOptions();
		List<String> dropDownValues = new ArrayList<>();
		for (WebElement item : allOptions) {
			dropDownValues.add(item.getText());
		}
		for (int i = 0; i < dropDownValues.size(); i++) {
			if (!dropdownExpected[i].equals(dropDownValues.get(i))) {
				Util.addExceptionToReport("Warning info mismatch", dropDownValues.get(i), dropdownExpected[i]);
			}
		}
	}

	String next = "Next";
	String previous = "Previous";

	/**
	 * @author rahul.kunjumon, this method is used to perform pagination and
	 *         verify if the next and previous buttons are enabled or disabled
	 *         according to the testcase
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void pagenationCheck(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		String valuesPassed[] = KWVariables.getVariables().get(val).split(",");
		Thread.sleep(2000);
		String actualInfoShowingBelowTable = getElementByUsing(valuesPassed[0]).getText();
		String AppPerPageStart = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf(valuesPassed[3]) + 8,
				actualInfoShowingBelowTable.indexOf(valuesPassed[4]) - 1);
		String AppPerPageEnd = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf(valuesPassed[4]) + 3,
				actualInfoShowingBelowTable.indexOf(valuesPassed[5]) - 1);
		String totalApplicationsInMyAppsStr = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf(valuesPassed[5]) + 3,
				actualInfoShowingBelowTable.indexOf(valuesPassed[6]) - 1);
		String expectedShowingInfoDisplayedBelowTable;
		int AppPerPageStartValue = Integer.parseInt(AppPerPageStart);
		int AppPerPageEndValue = Integer.parseInt(AppPerPageEnd);
		getElementByUsing(valuesPassed[2]).click();
		Thread.sleep(2000);
		waitForElement(getElementByUsing(valuesPassed[0]), 120);
		String actualInfoShowingBelowTableAfterClick = getElementByUsing(valuesPassed[0]).getText();
		if (valuesPassed[1].equalsIgnoreCase(previous)) {
			expectedShowingInfoDisplayedBelowTable = "Showing " + Integer.toString(AppPerPageStartValue - 10) + " to "
					+ Integer.toString(AppPerPageEndValue - 10) + " of " + totalApplicationsInMyAppsStr
					+ " entries\nPlease search date fields in the format MM/DD/YYYY.";
			if (!actualInfoShowingBelowTableAfterClick.equals(expectedShowingInfoDisplayedBelowTable)) {
				Util.addExceptionToReport(" info mismatch", actualInfoShowingBelowTableAfterClick,
						expectedShowingInfoDisplayedBelowTable);
			}

		} else if (valuesPassed[1].equalsIgnoreCase(next)) {
			expectedShowingInfoDisplayedBelowTable = "Showing " + Integer.toString(AppPerPageStartValue + 10) + " to "
					+ Integer.toString(AppPerPageEndValue + 10) + " of " + totalApplicationsInMyAppsStr
					+ " entries\nPlease search date fields in the format MM/DD/YYYY.";
			if (!actualInfoShowingBelowTableAfterClick.equals(expectedShowingInfoDisplayedBelowTable)) {
				Util.addExceptionToReport(" info mismatch", actualInfoShowingBelowTableAfterClick,
						expectedShowingInfoDisplayedBelowTable);

			}
		}
	}

	String visible = "CheckVisible";
	String notVisible = "CheckNotVisible";

	/**
	 * @author rahul.kunjumon, This method is to check if an element is visible
	 *         or not.
	 * @param val
	 * @throws Exception
	 */
	public void verifyElementVisibility(String val) throws Exception {
		String valuesPassed[] = val.split(",");
		if (valuesPassed[1].equalsIgnoreCase(visible)) {
			if (getElementByUsing(val).isDisplayed()) {
				Util.addExceptionToReport("Element Present", "Actual present", "Expected Absence");

			}
		} else if (valuesPassed[1].equalsIgnoreCase(notVisible)) {
			if (!getElementByUsing(val).isDisplayed()) {
				Util.addExceptionToReport("Element Present", "Actual present", "Expected Absence");

			}
		}
	}

	/**
	 * @author rahul.kunjumon, THIS METHOD IS TO VERIFY THE 1003 PAGE LOAN
	 *         INFORMATION
	 * @param val
	 * @throws Exception
	 */
	public void verifyTenOThreeLoanInfo(String val) throws Exception {
		String valuesPassed[] = KWVariables.getVariables().get(val).split(":");
		String MyAppsValues[] = valuesPassed[0].split(",");
		String TenOThreeValues[] = valuesPassed[1].split(",");
		List<String> MyAppsList = new ArrayList<String>();
		List<String> TenOThreeList = new ArrayList<String>();
		for (int i = 0; i < MyAppsValues.length; i++) {
			if (getElementByUsing(MyAppsValues[i]).getAttribute(valuesPassed[4]).equals(valuesPassed[5])) {
				MyAppsList.add(getElementByUsing(MyAppsValues[i]).getAttribute(valuesPassed[6]));
			} else {
				Select dropdown = new Select(getElementByUsing(MyAppsValues[i]));
				MyAppsList.add(dropdown.getFirstSelectedOption().getText());
			}
		}
		waitForElement(getElementByUsing(valuesPassed[2]), 15);
		getElementByUsing(valuesPassed[2]).click();
		getElementByUsing(valuesPassed[3]).click();
		Thread.sleep(2000);

		for (int i = 0; i < TenOThreeValues.length; i++) {
			if (getElementByUsing(TenOThreeValues[i]).getAttribute(valuesPassed[4]).equals(valuesPassed[5])) {
				TenOThreeList.add(getElementByUsing(TenOThreeValues[i]).getAttribute(valuesPassed[6]));

			} else {
				Select dropdown = new Select(getElementByUsing(TenOThreeValues[i]));
				TenOThreeList.add(dropdown.getFirstSelectedOption().getText());
			}
		}
		for (int i = 0; i < TenOThreeValues.length; i++) {
			if (!MyAppsList.get(i).equalsIgnoreCase(TenOThreeList.get(i))) {
				Util.addExceptionToReport("Values don't match", MyAppsList.get(i), TenOThreeList.get(i));
			}
		}
	}

	/**
	 * Compares the selected option
	 * 
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void compareSelectedOptionText(String val) throws Exception, IOException, TwfException {
		String[] value = KWVariables.getVariables().get(val).split(",");
		Select dropdown = new Select(getElementByUsing(value[0]));
		String ActualValue = dropdown.getFirstSelectedOption().getText();
		if (!value[1].equals(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}

	/**
	 * Converts the date format
	 * 
	 * @param Value
	 * @throws ParseException
	 * @throws BiffException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void convertDateFormat(String Value)
			throws ParseException, BiffException, IOException, TwfException, InterruptedException {
		String actualDate = getElementByUsing(Value).getText().trim();
		Thread.sleep(1000);
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); // 9/5/2018
																	// to
																	// 09/05/2018
		Date date = (Date) formatter.parse(actualDate);
		SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd/yyyy");
		String finalDate = newFormat.format(date);
		Date dateToday = new Date();
		String currentDate = new SimpleDateFormat("MM/dd/yyyy").format(dateToday);
		if (!finalDate.equals(currentDate)) {
			Util.addExceptionToReport("Warning info mismatch", finalDate, currentDate);
		}
	}

	/**
	 * Verifies the selected radio button
	 * 
	 * @param values
	 * @throws Exception
	 */
	public void verifyandSelectRadioG2G(String values) throws Exception {
		if (values != null) {
			String[] args = KWVariables.getVariables().get(values).split(":");
			if (args[1].equalsIgnoreCase("verify")) {
				List<WebElement> radioType = getElementByUsing(args[0]).findElements(By.xpath(args[2]));
				for (WebElement e : radioType) {
					if (e.isSelected()) {
						guildToGuildRft = true;
						System.out.println("Selected Radio Button = " + e.getAttribute("id"));
					}
				}
			} else {
				List<WebElement> radioType = getElementByUsing(args[0]).findElements(By.xpath(args[2]));
				for (WebElement e : radioType) {
					if (e.getText().equals(args[1])) {
						e.click();
						guildToGuildRft = true;
					}
				}
			}
		}
	}

	public void waitForPageLoaded() throws TwfException {
		System.out.println();
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		System.out.println("pageLoadCondition : " + pageLoadCondition.toString());
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	public void waitUntilElementInvisibility(String values) throws Exception {
		// System.out.println("****************** waitUntilElementInvisibility
		// ********************");
		String[] args = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();

		/*
		 * if(driver.findElements(By.xpath(args[1])).size()>0 &&
		 * driver.findElement(By.xpath(args[1])).isDisplayed()){
		 * System.out.println("Inside if !!!!!!!!!!!!!");
		 * System.out.println("Wait for element to disapper :: "+driver.
		 * findElement(By.xpath(args[1])).getText()); WebDriverWait wait = new
		 * WebDriverWait(driver, Integer.parseInt(args[0]));
		 * wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(
		 * args[1])));
		 * //wait.until(ExpectedConditions.invisibilityOfElementWithText(By.
		 * xpath(args[1]), args[2])); }
		 */

		// System.out.println("Done!!!!!!!!!");
	}

	/**
	 * Perform tab out keyboard event
	 * 
	 * @param values
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void performTabOut(String values) throws Exception, IOException, TwfException {
		System.out.println("****************** performTabOut ********************");
		System.out.println("values" + values);
		driver = DriverFactory.getDriver();
		Thread.sleep(2000);
		getElementByUsing(values).sendKeys(Keys.TAB);
		Thread.sleep(1000);
	}

	public void compareSelectedOptionValue(String val) throws Exception, IOException, TwfException {
		Select dropdown = new Select(getElementByUsing(val));
		String ActualValue = dropdown.getFirstSelectedOption().getText().trim();
		if (!selectedOption_2.equalsIgnoreCase(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, selectedOption_2);
		}
	}

	/**
	 * @author rahul.kunjumon, this method captures the selected option and
	 *         stores it in a static variable
	 * @param val
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void getNewSelectedOption(String val) throws Exception, IOException, TwfException {
		Select dropdown = new Select(getElementByUsing(val));
		selectedOption_2 = dropdown.getFirstSelectedOption().getText().trim();
	}

	public void verifyLeadStatus(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("****************** verifyLeadStatus ********************");
		String valuesPassed[] = KWVariables.getVariables().get(val).split(";");
		System.out.println(valuesPassed);
		Thread.sleep(2000);
		Select dropdown = new Select(getElementByUsing(valuesPassed[3]));
		dropdown.selectByValue(valuesPassed[4]);
		Thread.sleep(2000);
		String actualInfoShowingBelowTable = getElementByUsing(valuesPassed[0]).getText();
		String totalApplicationsInMyApps = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf(valuesPassed[1]) + 3,
				actualInfoShowingBelowTable.indexOf(valuesPassed[2]) - 1);
		System.out.println("totalApplicationsInMyApps :" + totalApplicationsInMyApps);
		Thread.sleep(2000);
		Select dropdown1 = new Select(getElementByUsing(valuesPassed[3]));
		dropdown1.selectByValue(valuesPassed[5]);
		Thread.sleep(2000);
		actualInfoShowingBelowTable = getElementByUsing(valuesPassed[0]).getText();
		String totalApplicationsInSendToMyKeyStatus = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf(valuesPassed[1]) + 3,
				actualInfoShowingBelowTable.indexOf(valuesPassed[2]) - 1);
		System.out.println("totalApplicationsInSendToMyKeyStatus :" + totalApplicationsInSendToMyKeyStatus);
		Thread.sleep(2000);
		Select dropdown2 = new Select(getElementByUsing(valuesPassed[3]));
		dropdown2.selectByValue(valuesPassed[6]);
		Thread.sleep(2000);
		actualInfoShowingBelowTable = getElementByUsing(valuesPassed[0]).getText();
		String totalApplicationsInLeadStatus = actualInfoShowingBelowTable.substring(
				actualInfoShowingBelowTable.indexOf(valuesPassed[1]) + 3,
				actualInfoShowingBelowTable.indexOf(valuesPassed[2]) - 1);
		System.out.println("totalApplicationsInLeadStatus :" + totalApplicationsInLeadStatus);

		if (totalApplicationsInMyApps.contains(",")) {
			totalApplicationsInMyApps = totalApplicationsInMyApps.replace(",", "");
		}

		if (totalApplicationsInLeadStatus.contains(",")) {
			totalApplicationsInLeadStatus = totalApplicationsInLeadStatus.replace(",", "");
		}

		if (totalApplicationsInSendToMyKeyStatus.contains(",")) {
			totalApplicationsInSendToMyKeyStatus = totalApplicationsInSendToMyKeyStatus.replace(",", "");
		}
		int ApplicationsInMyApps = Integer.parseInt(totalApplicationsInMyApps);
		int ApplicationsInLeadStatus = Integer.parseInt(totalApplicationsInLeadStatus);
		int ApplicationsInSendToMyKeyStatus = Integer.parseInt(totalApplicationsInSendToMyKeyStatus);
		int totalNumberofAppsInLeadStatus = (ApplicationsInMyApps) - (ApplicationsInSendToMyKeyStatus);
		int totalNumberofAppsInSendToMyKeyStatus = (ApplicationsInMyApps) - (ApplicationsInLeadStatus);
		if (ApplicationsInLeadStatus != totalNumberofAppsInLeadStatus) {
			Util.addExceptionToReport("Warning info mismatch", Integer.toString(ApplicationsInLeadStatus),
					Integer.toString(totalNumberofAppsInLeadStatus));
		}
		if (ApplicationsInSendToMyKeyStatus != totalNumberofAppsInSendToMyKeyStatus) {
			Util.addExceptionToReport("Warning info mismatch", Integer.toString(ApplicationsInSendToMyKeyStatus),
					Integer.toString(totalNumberofAppsInLeadStatus));
		}
	}

	/**
	 * Enter mandatory values for Refinance Loan
	 * 
	 * @param val
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void mandatoryFiledsRefinanceLoan(String val)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("****************** refinanceLoan ********************");
		String valuesPassed[] = KWVariables.getVariables().get(val).split(";");
		Select dropdown = new Select(getElementByUsing(valuesPassed[2]));
		String ActualValue = dropdown.getFirstSelectedOption().getText().trim();
		if (ActualValue.equalsIgnoreCase(valuesPassed[0]) || ActualValue.equalsIgnoreCase(valuesPassed[5])) {
			Select RefiType = new Select(getElementByUsing(valuesPassed[3]));
			int index = Integer.parseInt(valuesPassed[1]);
			RefiType.selectByIndex(index);
			Thread.sleep(2000);
			WebElement checkbox = getElementByUsing(valuesPassed[4]);
			System.out.println("--------------------------->" + checkbox.isSelected());
			if (!checkbox.isSelected()) {
				checkbox.click();
			}
		}
	}

	// 1::2::3
	public void verifyTheDefaultFees(String value)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("****************** verifyTheDefaultFees ********************");
		String val[] = KWVariables.getVariables().get(value).split("::");
		String storedValue[] = val[1].split(",");
		String textValue[] = val[0].split(",");
		String actualValue, defaultValue;
			for (int i = 1; i < storedValue.length; i++) {
				System.out.println(i);
				actualValue = MyTestCaseExecuter.stepobject.getKwValueVariables().get(storedValue[i]);
				defaultValue = getElementByUsing(textValue[i]).getText();
				if (!actualValue.equals(defaultValue)){
					System.out.println(
							textValue[i] + "-------->" + defaultValue + "----stored------" + actualValue);
					Util.addExceptionToReport("Warning info mismatch", actualValue , defaultValue);
					}
			}
	}

	public void verifyTheMilitaryIncomeValue(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		String val[] = KWVariables.getVariables().get(value).split(":");
		String storedValue[] = val[1].split(",");
		String actualValue[] = val[0].split(",");
		String actual, expected;
		System.out.println("-------------storedValue.length---------------" + storedValue.length);
		for (int i = 0; i < storedValue.length; i++) {
			
			expected = MyTestCaseExecuter.stepobject.getKwValueVariables().get(storedValue[i]);
			actual = getElementByUsing(actualValue[i]).getAttribute(val[2]);
			System.out.println("-------expected--------" + expected + "---------actual-------" + actual);
			if(!actual.equals(expected))
				Util.addExceptionToReport("Warning info mismatch",actual , expected);
		}
	}

	public void enterFirstPaymentDate(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException 
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();         
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date nextMonthFirstDay = calendar.getTime();
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date nextMonthLastDay = calendar.getTime();
		System.out.println(df.format(nextMonthFirstDay));
		String strDate=df.format(nextMonthFirstDay);
		getElementByUsing(value).clear();
		getElementByUsing(value).sendKeys(strDate);
	}
	
	public void clickOkOnAlertpopup(String values)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		System.out.println("*************** ClickOkOnAlertpopup ******************");
		WebDriver driver = DriverFactory.getDriver();
		getElementByUsing(values).click();
		Thread.sleep(3000);
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			System.out.println("Alert text ==> "+alert.getText());
		} catch (Exception e) {
			System.out.println("Alert not exists");
		}
	}

	public void clickUsingJavascript(String values) throws TwfException, InterruptedException, BiffException, IOException {
        driver = DriverFactory.getDriver();
        JavascriptExecutor js=(JavascriptExecutor)driver;
        WebElement link=getElementByUsing(values);
        js.executeScript("arguments[0].click();",link );
    }
	
	@Override
	public void checkPage() {
		// TODO Auto-generated method stub
	}
}
