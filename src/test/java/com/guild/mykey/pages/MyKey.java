package com.guild.mykey.pages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.guild.mykey.util.*;
import com.tavant.base.DriverFactory;
import com.tavant.base.WebPage;
import com.tavant.kwutils.KWVariables;
import com.tavant.utils.TwfException;
import jxl.read.biff.BiffException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class MyKey extends WebPage {
	WebDriver driver;
	int elementDisplayTimeOut = 30;
	static String borrowerName;
	static boolean guildToGuildRft = false;
	public static String loanNumber;
	public static String branchCode;
	public static String applicationId;
	static String respaDate;
	static ArrayList<String> applicationValues;
	static ArrayList<String> columnNames;
	public static ArrayList<String> selectedDropdownOptions = new ArrayList<String>();
	static String selectedOption;
	static String purposeOfLoan;
	static String parenthandle;
	static String selectedLoanProgram;
	static String loanProgramId;
	static String appraisalValue;
	static String saveAndContinueBtn, loanProgramDD;
	static String submitToUnderwriting, credit_val, suCreditRtbn, suPPropertyRbtn, suFullRbtn,submitToUnderwritingType;
	static String property_val, full_val, purchase_val;
	static String ssn1_txb, ssn2_txb, ssn3_txb, homePhoneNumber_txb;
	static String purposeOfLoan_dd, reoSubjectProperty_No_rbtn, reoSubjectProperty_Yes_rbtn;

	/**
	 * Verifies the login functionality with valid user credentials
	 * 
	 * @author muni.reddy
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyLogin(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* verifyLogin(String values) ************* ");
		String params = KWVariables.getVariables().get(values);
		String args[] = params.split(":");
		getElementByUsing(args[0]).clear();
		getElementByUsing(args[0]).sendKeys(args[5]);
		Thread.sleep(1000);
		getElementByUsing(args[1]).clear();
		getElementByUsing(args[1]).sendKeys(args[6]);
		getElementByUsing(args[2]).click();
		// Wait for Clear Filters button
		waitForElement(getElementByUsing(args[3]), elementDisplayTimeOut);
		String homePageMyAccLink = getElementByUsing(args[4]).getText();
		if (!homePageMyAccLink.equalsIgnoreCase(args[8])) {
			Util.addExceptionToReport("Login Failed", homePageMyAccLink, args[8]);
		}
	}

	/**
	 * Gets the window handles and switches between the windows
	 * 
	 * @author muni.reddy
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public void switchToWindow(String values) throws TwfException, InterruptedException {
		System.out.println("*********** switchToWindow *********** ");
		WebDriver driver = DriverFactory.getDriver();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			System.out.println("Switch to window is done");
		}
	}

	public void getBranchAndApplicationNumber(String value)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("******************** getBranchAndApplicationNumber **********************");
		applicationValues = new ArrayList<String>();
		driver = DriverFactory.getDriver();
		String args = KWVariables.getVariables().get(value);
		String borrowerInfo = getElementByUsing(args).getText().replace("   ", " ").trim();
		if (borrowerInfo.length() > 1) {
			String loanInfo[] = borrowerInfo.split(" ");
			branchCode = loanInfo[1].split("-")[0];
			applicationId = loanInfo[1].split("-")[1];
			borrowerName = loanInfo[2].replace(",", "") + " " + loanInfo[3];
			applicationValues.add(branchCode);
			applicationValues.add(applicationId);
			applicationValues.add(borrowerName);
			System.out.println("applicationValues : " + applicationValues);
		}
	}

	public void storeLoanDetails(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException {
		System.out.println("******************** storeLoanDetails **********************");
		String colHeader = "";
		String rowValue = "";
		columnNames = new ArrayList<String>();
		applicationValues = new ArrayList<String>();
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		String borrowerInfo = getElementByUsing(args[0]).getText().trim();
		List<WebElement> webList = driver.findElements(By.xpath(args[1]));
		columnNames.add("Loan Program");
		columnNames.add("Purpose of Loan");
		columnNames.add("Status");
		columnNames.add("Branch Code");
		columnNames.add("Application Id");
		columnNames.add("Borrower Name");
		applicationValues.add(selectedLoanProgram);
		applicationValues.add(purposeOfLoan);
		applicationValues.add(args[3]);

		if (borrowerInfo.length() > 1) {
			System.out.println("branchCode : applicationId : borrowerName ==> " + branchCode + " : " + applicationId
					+ " : " + borrowerName);
			applicationValues.add(branchCode);
			applicationValues.add(applicationId);
			applicationValues.add(borrowerName);
		}

		for (int i = 1; i <= webList.size(); i++) {
			List<WebElement> headerAllValues = driver.findElements(By.xpath(args[1] + "[" + i + args[2]));
			for (int j = 1; j <= headerAllValues.size(); j++) {
				if ((j % 2) != 0) {
					colHeader = driver.findElement(By.xpath(args[1] + "[" + i + args[2] + "[" + j + "]")).getText()
							.replace(":", "").trim();
					if (!(colHeader.equalsIgnoreCase("Loan Program") || colHeader.equalsIgnoreCase("Purpose of Loan")
							|| colHeader.equalsIgnoreCase("Status"))) {
						columnNames.add(colHeader);
					}
				} else {
					rowValue = driver.findElement(By.xpath(args[1] + "[" + i + args[2] + "[" + j + "]")).getText();
					if (rowValue.contains(",")) {
						rowValue = rowValue.replace(",", "");
					}
					if (!(colHeader.equalsIgnoreCase("Loan Program") || colHeader.equalsIgnoreCase("Purpose of Loan")
							|| colHeader.equalsIgnoreCase("Status"))) {
						applicationValues.add(rowValue);
					}
				}
			}
		}
		System.out.println("applicationValues ==> " + applicationValues);
	}

	/**
	 * Search dynamically generated application id
	 * @author muni.reddy
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public void searchApplication(String values) throws Exception {
		System.out.println("************ searchApplication ************ " + applicationId);
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String args[] = KWVariables.getVariables().get(values).split(":");
		waitForElement(getElementByUsing(args[0]), (elementDisplayTimeOut * 2));
		
		if (args[3].equalsIgnoreCase("underwritter")) {
			getElementByUsing(args[0]).clear();
			getElementByUsing(args[0]).sendKeys(branchCode);
			getElementByUsing(args[1]).clear();
			getElementByUsing(args[1]).sendKeys(applicationId);
			getElementByUsing(args[2]).click();
			Thread.sleep(1000);
		} else {
			getElementByUsing(args[0]).clear();
			getElementByUsing(args[0]).sendKeys(applicationId);
			Thread.sleep(1000);
			getElementByUsing(args[1]).click();
			Thread.sleep(1000);
		}
	}

	public void getCurrentWindowhandle(String args) throws TwfException {
		System.out.println("getCurrentWindowhandle ********** ");
		driver = DriverFactory.getDriver();
		parenthandle = driver.getWindowHandle();
		System.out.println("getCurrentWindowhandle :: " + parenthandle);
	}

	public void switchToParentWindow(String values)
			throws InterruptedException, BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("************ switchToParentWindow *********");
		String pHandle;
		driver = DriverFactory.getDriver();
		pHandle = parenthandle;
		driver.switchTo().window(pHandle);
		Thread.sleep(500);
	}

	/**
	 * Get date based on the input date format and the number of days
	 * @author muni.reddy
	 * @param dateFormat
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws BiffException
	 * @throws TwfException
	 * @throws InterruptedException
	 */
	public String enterClosingDate(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException, InterruptedException {
		System.out.println("************* enterClosingDate *******************" + values);
		String args[] = KWVariables.getVariables().get(values).split(":");
		String emplymentStartDate = Util.getDateToSearch(args[0], args[1], Integer.parseInt(args[2]));

		if (args.length > 4) {
			String val[] = emplymentStartDate.split("/");
			if (args[4].equalsIgnoreCase("dd")) {
				emplymentStartDate = val[1];
			} else if (args[4].equalsIgnoreCase("mm")) {
				emplymentStartDate = val[0];
			} else if (args[4].equalsIgnoreCase("yyyy")) {
				emplymentStartDate = val[2];
			} else if (args[4].equalsIgnoreCase("mmyyyy")) {
				emplymentStartDate = val[0] + val[2];
			}
		}
		emplymentStartDate = emplymentStartDate.trim();

		if (getElementByUsing(args[3]).isDisplayed()) {
			getElementByUsing(args[3]).clear();
			Thread.sleep(500);
			getElementByUsing(args[3]).sendKeys(emplymentStartDate);
			Thread.sleep(1000);
		}
		return emplymentStartDate;
	}

	/**
	 * Get date based on the format and the numbers of days requested by adding
	 * subtracting
	 * 
	 * @author muni.reddy
	 * @param strDateFormat
	 * @param days
	 * @return
	 * @throws TwfException
	 * @throws IOException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 */
	public void enterFirstPaymentDate(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("************* enterFirstPaymentDate *******************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		LocalDate date = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(args[0]);
		String firstPaymnetStartDate = formatter.format(date.with(TemporalAdjusters.firstDayOfNextMonth()));
		getElementByUsing(args[3]).sendKeys(firstPaymnetStartDate);
	}

	public void selectLoanProgramAndHandleAlert(Map<String, String> dataPoolArgs)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("*************** selectAndHandleAlert ****************");
		driver = DriverFactory.getDriver();
		waitForElement(getElementByUsing(saveAndContinueBtn), (elementDisplayTimeOut * 2));
		String optionToSelect = dataPoolArgs.get(loanProgramDD);
		loanProgramId = optionToSelect;
		Select ddOptions = new Select(getElementByUsing(loanProgramDD));
		try {
			ddOptions.selectByValue(optionToSelect);
			Thread.sleep(1000);
			Alert alert = driver.switchTo().alert();
			Thread.sleep(1000);
			alert.accept();
			Thread.sleep(500);

		} catch (Exception e) {
			System.out.println("No Alert!!!!!!!!!!");
		}
	}

	public void selectOptions(String val) throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("***************** selectOptions *******************");
		String args[] = KWVariables.getVariables().get(val).split(":");
		Select opt = new Select(getElementByUsing(args[0]));
		opt.selectByVisibleText(args[1]);
	}

	public void storeSelectedOption(String val)
			throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("***************** storeSelectedOption *******************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(val).split(":");
		Select opt = new Select(getElementByUsing(args[0]));
		purposeOfLoan = opt.getFirstSelectedOption().getText().trim();
		if (driver.findElements(By.xpath(args[1])).size() > 0) {
			if (driver.findElement(By.xpath(args[1])).isDisplayed()) {
				driver.findElement(By.xpath(args[1])).click();
			}
		}
	}

	public void clickOnTakeActionLink(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("********************clickOnTakeActionLink********************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		List<WebElement> aList = driver.findElements(By.xpath(args[0]));
		int actualBranchCode = 0;
		int actualApplicationId = 0;
		for (int i = 2; i < (aList.size()); i++) {
			actualBranchCode = Integer
					.parseInt(driver.findElement(By.xpath(args[0] + "[" + i + args[1])).getText().trim());
			actualApplicationId = Integer
					.parseInt(driver.findElement(By.xpath(args[0] + "[" + i + args[2])).getText().trim());
			if (Integer.parseInt(branchCode) == actualBranchCode
					&& Integer.parseInt(applicationId) == actualApplicationId) {
				driver.findElement(By.xpath(args[0] + "[" + i + args[3])).click();
				break;
			}
		}
	}

	public void selectMultipleRadioButtons(String values)
			throws Exception {
		System.out.println("******************** selectMultipleRadioButtons *****************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
	
		System.out.println("selectedLoanProgram ::credit_val ====>  "+selectedLoanProgram+" :: "+credit_val);
		if (!(selectedLoanProgram.equalsIgnoreCase("VA FIXED 30YR- 41") && credit_val.equalsIgnoreCase("Credit"))){
			List<WebElement> aList = driver.findElements(By.xpath(args[0]));
			int i = Integer.parseInt(args[4]);
			String attributeValue = "";
			if (!loanProgramId.equalsIgnoreCase("879")) {
				for (; i < aList.size(); i++) {
					attributeValue = driver.findElement(By.xpath(args[0] + "[" + i + "]")).getAttribute(args[3]).trim();
					if (!attributeValue.equalsIgnoreCase("text")) {
						String val=args[0] + "[" + i + args[1] + args[2];
						scrollToElement(val);
						driver.findElement(By.xpath(args[0] + "[" + i + args[1] + args[2])).click();
				}
				}
			}
		}

	}

	/**
	 * Creates loans CSV file
	 * 
	 * @author muni.reddy
	 * @param args
	 * @throws IOException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws TwfException
	 */
	public void createLoansCSVFile(String args)
			throws IOException, BiffException, InvalidFormatException, TwfException {
		System.out.println("************* createLoansCSVFile *************");
		String[] fileToCreateDetails = KWVariables.getVariables().get(args).split(":");

		FileWriter writer = null;
		String resultFolderDate = new SimpleDateFormat(fileToCreateDetails[0]).format(new Date())
				.replaceAll("[\\/\\-\\:]", "_");
		File currentResDir = new File(System.getProperty("user.dir") + File.separator + fileToCreateDetails[1]
				+ File.separator + fileToCreateDetails[2] + resultFolderDate);
		currentResDir.mkdirs();
		String newLoansCreatedCSVFilePath = currentResDir.getAbsolutePath() + File.separator + fileToCreateDetails[3];
		System.out.println(" newLoansCreatedCSVFilePath :  " + newLoansCreatedCSVFilePath);
		System.out.println("columnNames : applicationValues ====>  " + columnNames + " : " + applicationValues);

		try {
			boolean alreadyExists = new File(newLoansCreatedCSVFilePath).exists();
			if (!alreadyExists) {
				writer = new FileWriter(newLoansCreatedCSVFilePath, true);
				for (int i = 0; i < columnNames.size(); i++) {
					writer.write(columnNames.get(i).toString());
					writer.write(",");
				}
				writer.write("\n");

			} else {
				writer = new FileWriter(newLoansCreatedCSVFilePath, true);
			}
			for (int i = 0; i < applicationValues.size(); i++) {
				writer.write(applicationValues.get(i).toString().trim());
				writer.write(",");
			}
			writer.write("\n");
			System.out.println("Write success!");
		} catch (IOException e) {
			e.printStackTrace();
			writer.close();
		} finally {
			writer.close();
		}
	}

	public void clickOnContinueButton(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** clickOnContinueButton *************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		if (loanProgramId != null && loanProgramId.length() > 1 && !args[0].equalsIgnoreCase("Underwriting")) {
			System.out.println("args[0] " + args[0]);
			if (loanProgramId.equalsIgnoreCase("879") || loanProgramId.equalsIgnoreCase("2159")) {
				getElementByUsing(args[0]).click();
				waitForElement(getElementByUsing(args[1]), (elementDisplayTimeOut * 2));
			}
		}
	}

	public void verifyAppStatus(String values) throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("******************* verifyAppStatus *****************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		String actualAppStatus = getElementByUsing(args[0]).getText().trim();
		if (!args[1].equalsIgnoreCase(actualAppStatus)) {
			System.out.println("Application status mismatch !!!!!!!!!!!!!!");
			System.out.println("args[1] : actualAppStatus =====> " + args[1] + " : " + actualAppStatus);
		}

	}

	public void verifyStatusInAppHistory(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("******************* verifyStatusInAppHistory *****************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		waitForElement(getElementByUsing(args[1]), elementDisplayTimeOut);
		boolean appStatus = false;
		String actualAppStatus = "";
		String actualLatestStatus = driver.findElement(By.xpath(args[3] + "[" + 1 + args[4])).getText().trim().toUpperCase();
		String expectedStatus = args[2].toUpperCase();
		for (int i = 1; i < driver.findElements(By.xpath(args[3])).size(); i++) {
			actualAppStatus = driver.findElement(By.xpath(args[3] + "[" + i + args[4])).getText().trim().toUpperCase();
			if (actualAppStatus.contains(expectedStatus)) {
				appStatus = true;
				break;
			}
		}

		System.out.println("***************actualLatestStatus : "+actualLatestStatus);
		if (!appStatus) {
			Util.addExceptionToReport("App History page status mismatch", actualLatestStatus, expectedStatus);
		}
	}

	public void verifyRespaDate(String values) throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("**************** verifyRespaDate *****************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		String actualRrespaDate = getElementByUsing(args[0]).getText().trim();
		if (args[1].equalsIgnoreCase("RespaDate") && actualRrespaDate.length() < 4) {
			Util.addExceptionToReport("Respa date not created", actualRrespaDate, "Should have respa date");
		} else if (args[1].equalsIgnoreCase("NoRespaDate") && actualRrespaDate.length() > 4) {
			Util.addExceptionToReport("Respa date created", actualRrespaDate, "Should not have respa date");
		}
	}

	public String getAppraiserDetails(String args) throws IOException, BiffException, InvalidFormatException,
			TwfException, ParseException, InterruptedException {
		System.out.println("******** getAppraiserDetails **********");
		String[] values = KWVariables.getVariables().get(args).split(":");
		Date dateToday = new Date();
		List<WebElement> ColumnDataA = DriverFactory.getDriver().findElements(By.xpath(values[0] + values[2]));
		List<WebElement> ColumnDataB = DriverFactory.getDriver().findElements(By.xpath(values[0] + values[3]));
		List<WebElement> ColumnDataC = DriverFactory.getDriver().findElements(By.xpath(values[0] + values[1]));
		if (ColumnDataA.size() > 0) {
			for (int i = 0; i < ColumnDataA.size(); i++) {
				String columnValueA = ColumnDataA.get(i).getText().trim();
				String columnValueB = ColumnDataB.get(i).getText().trim();
				Date dateA = new SimpleDateFormat(values[4]).parse(columnValueA);
				Date dateB = new SimpleDateFormat(values[4]).parse(columnValueB);
				if (dateA.after(dateToday) && dateB.after(dateToday)) {
					appraisalValue = ColumnDataC.get(i).getText().trim();
					DriverFactory.getDriver().findElement(By.xpath(values[0] + "[" + (i + 1) + "]" + values[1])).click();
					break;
				}
			}
		}
		return appraisalValue;
	}

	public void selectPurchaseRadioButton(String values)
			throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("***************** selectPurchaseRadioButton ************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		if (!args[0].equalsIgnoreCase(selectedLoanProgram)) {
			if (args[1].equalsIgnoreCase(purposeOfLoan)) {
				try{
				getElementByUsing(args[2]).click();
				}
				catch (Exception e) {
					getElementByUsing(args[3]).click();
				}
			}
		}
	}

	public void selectSubjectProperty(Map<String, String> dataPoolArgs)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("*************** selectLoanProgramAndHandleAlert ****************");
		driver = DriverFactory.getDriver();
		String optionToSelect = dataPoolArgs.get(purposeOfLoan_dd);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		if (optionToSelect.equalsIgnoreCase(purchase_val)) {
			System.out.println("-------------Entered if---------------");
			getElementByUsing(reoSubjectProperty_No_rbtn).click(); // No
		} else {
			getElementByUsing(reoSubjectProperty_Yes_rbtn).click(); // Yes
		}
	}

public void getRatiosAndEnter(String values)
			throws TwfException, BiffException, IOException, InvalidFormatException, InterruptedException {
		System.out.println("************** getRatiosAndEnter ************");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(args[0])).size() > 0) {
			String housingRatio = getElementByUsing(args[1]).getText();
			String dtiRatio = getElementByUsing(args[2]).getText();
			housingRatio = housingRatio.substring(housingRatio.lastIndexOf(":") + 1, housingRatio.length()).trim();
			dtiRatio = dtiRatio.substring(dtiRatio.lastIndexOf(":") + 1, dtiRatio.length()).trim();
			getElementByUsing(args[3]).sendKeys(housingRatio);
			getElementByUsing(args[4]).sendKeys(dtiRatio);
			Thread.sleep(100);
		}
	}

	/**
	 * Selects submitting to Submit to Underwriting options
	 * 
	 * @param dataPoolArgs
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void selectSubmitToUnderwritingRadioButton(Map<String, String> dataPoolArgs)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** selectSubmitToUnderwritingRadioButton *************");
		// Get data from Data pool column "RepoElements"
		submitToUnderwritingType = dataPoolArgs.get(submitToUnderwriting);
		if (submitToUnderwritingType != null && submitToUnderwritingType.length() > 1) {
			if (submitToUnderwritingType.equalsIgnoreCase(credit_val)) {
				getElementByUsing(suCreditRtbn).click();
			} else if (submitToUnderwritingType.equalsIgnoreCase(property_val)) {
				getElementByUsing(suPPropertyRbtn).click();
			} else if (submitToUnderwritingType.equalsIgnoreCase(full_val)) {
				getElementByUsing(suFullRbtn).click();
			}
		}
	}

	/**
	 * Select radio button
	 * 
	 * @param values
	 * @throws TwfException
	 * @throws BiffException
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void selectRadioButton(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("*************** selectRadioButton ***********");
		String args[] = KWVariables.getVariables().get(values).split(":");
		driver=DriverFactory.getDriver();
		if(driver.findElements(By.xpath(args[0])).size()>0){
			if (getElementByUsing(args[1]).isDisplayed()) {
				getElementByUsing(args[1]).click();
			}
		}
	}

	/**
	 * @author rahul.kunjumon, this method is to handle any popups coming in the
	 *         application.
	 * @param val
	 * @return
	 * @throws Exception
	 */

	public String unlockApplication(String val) throws Exception {
		System.out.println("****************** unlockApplication ***********************");
		WebDriver driver = DriverFactory.getDriver();
		String[] value = val.split(",");
		getElementByUsing(value[1]).click();
		Thread.sleep(1000);
		try {
			Alert alert = driver.switchTo().alert();
			Thread.sleep(1000);
			String alertMessage = alert.getText();
			if (value[0].equalsIgnoreCase("OK")) {
				alert.accept();
				// Store the current window handle
				String winHandleBefore = driver.getWindowHandle();
				System.out.println("1");
				Thread.sleep(1000);
				// click on unlock app button on the page
				getElementByUsing(value[2]).click();
				System.out.println("2");
				// switch to new window
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
				System.out.println("3");
				// click on unlock app button on the new page
				getElementByUsing(value[3]).click();
				// Switch back to original browser (first window)
				System.out.println("4");
				driver.switchTo().window(winHandleBefore);
				getElementByUsing(value[1]).click();
			}
			if (value[0].equalsIgnoreCase("CANCEL")) {
				alert.dismiss();
			}
			return (alertMessage);
		} catch (Exception e) {
			return "No alert found";
		}
	}

	public void storeLoanProgram(String values)
			throws BiffException, IOException, TwfException, InvalidFormatException, InterruptedException {
		System.out.println("*************** storeLoanProgram ****************");
		String args = KWVariables.getVariables().get(values);
		driver = DriverFactory.getDriver();
		Select ddOptions = new Select(getElementByUsing(args));
		selectedLoanProgram = ddOptions.getFirstSelectedOption().getText().trim();
		selectedLoanProgram = selectedLoanProgram.replace("  ", "");
	}

	public void getValuesForDataPoolsMethods(String values) throws BiffException, InvalidFormatException, IOException, TwfException {
		System.out.println("************************ getValuesForDataPoolsMethods *******************");
		String dpValues[] = KWVariables.getVariables().get(values).split(":");
		ssn1_txb = dpValues[0];
		ssn2_txb = dpValues[1];
		ssn3_txb = dpValues[2];
		homePhoneNumber_txb = dpValues[3];
		saveAndContinueBtn = dpValues[4];
		loanProgramDD = dpValues[5];
		submitToUnderwriting = dpValues[6];
		suCreditRtbn = dpValues[7];
		suPPropertyRbtn = dpValues[8];
		suFullRbtn = dpValues[9];
		purposeOfLoan_dd = dpValues[10];
		WebDriver driver = DriverFactory.getDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		reoSubjectProperty_No_rbtn = dpValues[11];
		reoSubjectProperty_Yes_rbtn = dpValues[12];
		credit_val = dpValues[13];
		property_val = dpValues[14];
		full_val = dpValues[15];
		purchase_val = dpValues[16];
	}

	public void handleSSNAlert(Map<String, String> dataPoolArgs) throws BiffException, IOException, TwfException, InterruptedException {
		System.out.println("****************** handleSSNAlert ********************");
		driver = DriverFactory.getDriver();
		String ssn1 = dataPoolArgs.get(ssn1_txb);
		String ssn2 = dataPoolArgs.get(ssn2_txb);
		String ssn3 = dataPoolArgs.get(ssn3_txb);
		
		try {
			getElementByUsing(ssn1_txb).sendKeys(ssn1);
			Thread.sleep(500);
			getElementByUsing(ssn2_txb).sendKeys(ssn2);
			Thread.sleep(500);
			getElementByUsing(ssn3_txb).sendKeys(ssn3);
			Thread.sleep(500);
			getElementByUsing(homePhoneNumber_txb).click();
			Thread.sleep(1000);
			
			WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.alertIsPresent());

			Alert alert = driver.switchTo().alert();
			String actualAlertText = alert.getText().trim();
			Thread.sleep(2000);
			System.out.println("Alert text : " + actualAlertText);
			alert.accept();
			Thread.sleep(1000);

		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
	}

	public void checkIfIsDisplayed(String Values) throws Exception {
		System.out.println("****************** checkIfIsDisplayed ********************");
		String args[] = Values.split(",");
		if (getElementByUsing(args[0]).isDisplayed()) {
			getElementByUsing(args[0]).clear();
			getElementByUsing(args[0]).sendKeys(args[1]);
		}
	}
	
	/**
	 * Enter value to Simultaneous text box
	 * @param dataPoolArgs
	 * @throws Exception
	 */
	public void enterValueToSimultaneousTxb(Map<String, String> dataPoolArgs) throws Exception {
		System.out.println("****************** enterValueToSimultaneousTxb ********************");
		String value=dataPoolArgs.get("MyK_NRF_SimultaneousIssue_txb");
		System.out.println("Simultaneous text field value :: "+value);
		if (getElementByUsing("MyK_NRF_SimultaneousIssue_txb").isDisplayed()) {
			getElementByUsing("MyK_NRF_SimultaneousIssue_txb").clear();
			getElementByUsing("MyK_NRF_SimultaneousIssue_txb").sendKeys(value);
		}
	}

	public void enterSellerInformation(String values)
			throws TwfException, BiffException, IOException, InterruptedException, InvalidFormatException {
		System.out.println("****************** enterSellerInformation ********************");
		String repoElements[] = KWVariables.getVariables().get(values).split("::")[0].split(":");
		String val[] = KWVariables.getVariables().get(values).split("::")[1].split(":");
		
		WebDriver driver = DriverFactory.getDriver();
		String winHandleBefore = driver.getWindowHandle();
		getElementByUsing(repoElements[0]).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		waitForElement(getElementByUsing(repoElements[1]), (elementDisplayTimeOut * 4));
		getElementByUsing(repoElements[1]).click();
		Thread.sleep(1000);
		
		if(getElementByUsing(repoElements[2]).isEnabled()){
			getElementByUsing(repoElements[2]).clear();
			getElementByUsing(repoElements[2]).sendKeys(val[0]);
			
			getElementByUsing(repoElements[3]).clear();
			getElementByUsing(repoElements[3]).sendKeys(val[1]);
			
			getElementByUsing(repoElements[4]).clear();
			getElementByUsing(repoElements[4]).sendKeys(val[2]);
			
			new Select(getElementByUsing(repoElements[5])).selectByValue(val[3]);
			
			getElementByUsing(repoElements[6]).clear();
			getElementByUsing(repoElements[6]).sendKeys(val[4]);
			Thread.sleep(1000);
		}
		
		getElementByUsing(repoElements[7]).click();
		Thread.sleep(1500);
		driver.close();
		driver.switchTo().window(winHandleBefore);
	}

	public void checkRadioButton(String values) throws Exception {
		System.out.println("****************** checkRadioButton ********************");
		String args = KWVariables.getVariables().get(values);
		Thread.sleep(2000);
		if (getElementByUsing(args).isDisplayed()) {
			scrollToElement(args);
			getElementByUsing(args).click();
		}
	}

	public void enterUnderwritterCreditScores(String values)
			throws TwfException, BiffException, IOException, InvalidFormatException {
		System.out.println("************** enterUnderwritterCreditScores ************");
		String args[] = KWVariables.getVariables().get(values).split(",");
		String repoElements[] = args[0].split(":");
		String val[] = args[1].split(":");
		driver = DriverFactory.getDriver();
		if (driver.findElements(By.xpath(repoElements[0])).size() > 0) {
			if (driver.findElement(By.name(repoElements[1])).isEnabled()
					&& driver.findElement(By.name(repoElements[1])).isDisplayed()) {
				getElementByUsing(repoElements[2]).sendKeys(val[0]);
				getElementByUsing(repoElements[3]).sendKeys(val[0]);
				getElementByUsing(repoElements[4]).sendKeys(val[0]);
				getElementByUsing(repoElements[5]).sendKeys(val[0]);
			}
			if (driver.findElement(By.name(repoElements[6])).isEnabled()
					&& driver.findElement(By.name(repoElements[6])).isDisplayed()) {
				getElementByUsing(repoElements[7]).sendKeys(val[1]);
				getElementByUsing(repoElements[8]).sendKeys(val[2]);
			}
		}
		if (driver.findElements(By.xpath(repoElements[9])).size() > 0) {
			if (driver.findElement(By.xpath(repoElements[9])).isDisplayed()) {
				getElementByUsing(repoElements[10]).click();

			}
		}
	}

	public void checkIfIsDisplayedAndClick(String Values) throws Exception {
		System.out.println("****************** checkIfIsDisplayedAndClick ********************");
		String args[] = Values.split(",");
		if (getElementByUsing(args[0]).isDisplayed()) {
			getElementByUsing(args[0]).click();
		}
	}

	public void waitUntilElementInvisibility(String values) throws Exception {
		System.out.println("****************** waitUntilElementInvisibility ********************");
		String[] args = KWVariables.getVariables().get(values).split(":");
		driver = DriverFactory.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(args[0]));
		wait.until(ExpectedConditions.invisibilityOfElementWithText(By.id(args[1]), args[2]));
		System.out.println("Done!!!!!!!!!");
	}
	
	
	public void selectUWActionNoticePrintOptionsCheckBox(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** selectUWActionNoticePrintOptionsCheckBox *************");
		String args[]=KWVariables.getVariables().get(values).split(":");
		if (submitToUnderwritingType != null && submitToUnderwritingType.length() > 1) {
			if (submitToUnderwritingType.equalsIgnoreCase(full_val)) {
				getElementByUsing(args[0]).click();
			} else if (submitToUnderwritingType.equalsIgnoreCase(credit_val)) {
				getElementByUsing(args[1]).click();
			}
		}
	}
	
	/*MYK_VA_EntitlementCodeDd:
	 * MYK_VA_ServiceBranch:
	 * MYK_VA_MilitaryStatus:
	 * MYK_VA_MI:MYK_VA_Amount:
	 * MyK_Save_And_Continue_btn,
	 * 
	 * 5
	 * :1
	 * :1
	 * :100
	 * :5000*/
	public void enterVASubmissionPackValues(String values)
			throws TwfException, BiffException, InvalidFormatException, IOException, InterruptedException {
		System.out.println("***************** enterVASubmissionPackValues *************");
		String args[]=KWVariables.getVariables().get(values).split(",");
		String repoId[]=args[0].split(":");
		String val[]=args[1].split(":");
		if (submitToUnderwritingType != null && submitToUnderwritingType.length() > 1) {
			if (!submitToUnderwritingType.equalsIgnoreCase(credit_val)) {
				Select entitlementCode_dd=new Select(getElementByUsing(repoId[0]));
				Select erviceBranch_dd=new Select(getElementByUsing(repoId[1]));
				getElementByUsing(repoId[2]).click();
				System.out.println("submitToUnderwritingType : credit_val : "+submitToUnderwritingType+" : "+credit_val);
				waitForElement(getElementByUsing(repoId[5]), (elementDisplayTimeOut * 4));
				entitlementCode_dd.selectByIndex(Integer.parseInt(val[0]));
				erviceBranch_dd.selectByIndex(Integer.parseInt(val[1]));
				//militaryStatus_dd.selectByIndex(Integer.parseInt(val[2]));
				getElementByUsing(repoId[3]).clear();
				getElementByUsing(repoId[3]).sendKeys(val[3]);
				getElementByUsing(repoId[4]).clear();
				getElementByUsing(repoId[4]).sendKeys(val[4]);
				getElementByUsing(repoId[5]).click();
			}
		}
	}
	
	
	public void verifyAppSubmittedWithoutApplicationEdits(String values) throws BiffException, InvalidFormatException, IOException, TwfException{
		System.out.println("**************** verifyAppSubmittedWithoutApplicationEdits *****************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		try{
		if(getElementByUsing(args[1]).isDisplayed())
		{
			String firstEditMessage=getElementByUsing(args[1]).getText().trim();
			System.out.println("****************** firstEditMessage ::: "+firstEditMessage);
		if(!purposeOfLoan.equalsIgnoreCase(args[3]) && firstEditMessage.contains(args[2]))
		{
			Util.addExceptionToReport("There are application edits", firstEditMessage, "There should not be any Errors in application edits");
		}
		}
		}catch (Exception e) {
			System.out.println("No edits and Warnings");
		}
	}
	
	public void enterOriginalCostOfLot(String values) throws TwfException, BiffException, InvalidFormatException, IOException{
		System.out.println("**************** enterOriginalCostOfLot *****************");
		driver = DriverFactory.getDriver();
		String args[] = KWVariables.getVariables().get(values).split(":");
		//getElementByUsing(args[0]).click();
		getElementByUsing(args[1]).clear();
		getElementByUsing(args[1]).sendKeys(args[2]);
	}
	
	public void handleSaveAndContinueBtnAlert(String values) throws BiffException, IOException, TwfException, InvalidFormatException {
		System.out.println("****************** handleSaveAndContinueBtnAlert ********************");
		driver = DriverFactory.getDriver();
		String args = KWVariables.getVariables().get(values);
		try {
			System.out.println("args :: "+args);
			getElementByUsing(args).click();
			Thread.sleep(1000);
			Alert alert = driver.switchTo().alert();
			String actualAlertText = alert.getText().trim();
			System.out.println("Alert text : " + actualAlertText);
			alert.accept();
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("No alert found !!!!!!!!");
		}
	}
	
	public void clickOkOnAlertpopup(String values)
			throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
		System.out.println("*************** ClickOkOnAlertpopup ******************");
		WebDriver driver = DriverFactory.getDriver();
		String params = KWVariables.getVariables().get(values);
		
		try {
			Thread.sleep(1000);
			getElementByUsing(params).click();
			Thread.sleep(1000);
			Alert alert = driver.switchTo().alert();
			alert.accept();
			System.out.println("Alert text ==> "+alert.getText());
		} catch (Exception e) {
			System.out.println("Alert not exists");
		}
	}

	/**
	 * Switch to window perform click action and switch back to parent
	 * @param values
	 * @throws Exception 
	 */
	public void switchToWindowAndSwitchBack(String values) throws Exception{
		System.out.println("********************* switchToWindowAndSwitchBack ***************************");
		driver = DriverFactory.getDriver();
        String mainWindow=driver.getWindowHandle();	
        System.out.println("Parent :: "+parenthandle);
        System.out.println("mainWindow :: "+mainWindow);
        String[] args = KWVariables.getVariables().get(values).split(":");
        getElementByUsing(args[0]).click();
        Thread.sleep(2000);
        Set<String> s1=driver.getWindowHandles();	
        System.out.println("s1 : "+s1);
        Iterator<String> i1=s1.iterator();		
        while(i1.hasNext())			
        {		
            String windowUniqueId=i1.next();		
            System.out.println("windowUniqueId : "+windowUniqueId);
            if(!windowUniqueId.equalsIgnoreCase(parenthandle) && !windowUniqueId.equalsIgnoreCase(mainWindow))			
            {    		
            	System.out.println("Inside if ********* ");
                   driver.switchTo().window(windowUniqueId);	
                   Thread.sleep(2000);
                   scrollToElement(args[1]);
                   getElementByUsing(args[1]).click();
                   Thread.sleep(2000);
            }		
        }		
            driver.switchTo().window(mainWindow);	
	}
	
	/**
	 * Scrolls to the web element
	 * 
	 * @param ele
	 * @throws Exception
	 * @throws IOException
	 * @throws TwfException
	 */
	public void scrollToElement(String ele) throws Exception, IOException, TwfException {
		System.out.println("***************** scrollToElement *****************");
		WebDriver driver = DriverFactory.getDriver();
		WebElement ele2 = null;
		try{
		ele2 = getElementByUsing(ele);
		}
		catch (Exception e) {
			ele2 = driver.findElement(By.xpath(ele));
		}
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(4000,0)", ele2);
		js.executeScript("window.scrollBy(4000,0)", ele2);
		js.executeScript("arguments[0].scrollIntoView(true);", ele2);
	}
	
	public void popupHandlerVerify(String val) throws Exception {
		String[] value = val.split(";");
		String ActualValue = popupHandler(value[0]);
		if (!value[1].contains(ActualValue)) {
			Util.addExceptionToReport("Warning info mismatch", ActualValue, value[1]);
		}
	}
	
	public String popupHandler(String val) throws Exception {
		System.out.println("***************** popupHandler **********************");
		WebDriver driver = DriverFactory.getDriver();
		String[] value = val.split(",");
		getElementByUsing(value[1]).click();
		Thread.sleep(2000);
		try {
			Alert alert = driver.switchTo().alert();
			Thread.sleep(2000);
			String alertMessage = alert.getText().trim();
			if (value[0].equalsIgnoreCase("OK")) {
				alert.accept();
			}
			if (value[0].equalsIgnoreCase("CANCEL")) {
				alert.dismiss();
			}
			return (alertMessage);
		} catch (Exception e) {
			return "No alert found";
		}
	}

	public void clickCheckboxIfNotSelected(String val) throws Exception {
		WebDriver driver = DriverFactory.getDriver();
		WebElement checkbox=getElementByUsing(val);
		if(!checkbox.isSelected())
			checkbox.click();
	}
	
			public void selectDropdownOption(String option) throws BiffException, InvalidFormatException, IOException, TwfException{
					System.out.println("---------------selectDropdownOption------------");

		String values[] = KWVariables.getVariables().get(option).split("::");
		WebElement we = getElementByUsing(values[0]);
		

		Util.selectDropdownOption(we, values[1], values[2]);
	}
			
			public void enterExsistingCase(String val) throws Exception {
				String values[] = KWVariables.getVariables().get(val).split(":");
				Random rand = new Random();
				int num = rand.nextInt(900000) + 10;
				String randomNumber = Integer.toString(num);
				System.out.println("auto" + num);
				String loanPurpose = getElementByUsing(values[1]).getText();
				if (loanPurpose.equalsIgnoreCase(values[0])) {
					getElementByUsing(values[2]).sendKeys(randomNumber);
				}
			}
	
			public void navigateToPageThroughHeaderTab(String values) throws TwfException, InterruptedException, BiffException, IOException, InvalidFormatException {
				String value[] = KWVariables.getVariables().get(values).split(",");
				System.out.println("--------------navigateToPageThroughHeaderTab-----------");
				System.out.println(values);
				System.out.println(value[0]);
				System.out.println(value[1]);
				driver = DriverFactory.getDriver();
				WebElement menu = getElementByUsing(value[0]);
				
				try{
				Actions action = new Actions(driver);
				action.moveToElement(menu).perform();
				WebElement submenu=getElementByUsing(value[1]);
				submenu.click();
					}
				
				catch (Exception e) {
					menu.click();
				}
		    }
			public void selectRadioBtn(String values) throws Exception
			{
				System.out.println("---------------selectRadioBtn------------");
				String value[] = values.split(":");
				try{
					scrollToElement(value[0]);
					WebElement rbtn1=getElementByUsing(value[0]);
					rbtn1.click();
				}catch (Exception e) {
					scrollToElement(value[1]);
					WebElement rbtn2=getElementByUsing(value[1]);
					rbtn2.click();
				}
			}
			public void performTabOut(String values) throws Exception, IOException, TwfException {
				System.out.println("*********************** performTabOut ******************************");
				driver = DriverFactory.getDriver();
				getElementByUsing(values).sendKeys(Keys.TAB);
			}

			
			public void enterTheValueIntoMortgageInsurance(String values) throws Exception, IOException, TwfException {
				System.out.println("*********************** enterTheValueIntoMortgageInsurance ******************************");
				String value[] = values.split(",");
				
				try{
					if(getElementByUsing(value[0]).isDisplayed()&&getElementByUsing(value[1]).isDisplayed())
					{
						getElementByUsing(value[0]).clear();
						getElementByUsing(value[0]).sendKeys(value[2]);
						getElementByUsing(value[1]).clear();
						getElementByUsing(value[1]).sendKeys(value[3]);
					}
					
					
				}catch (Exception e) {
					System.out.println("Values are disabled");
					
				}
			}
			
			public void selectDropDownValue(String values) throws Exception {
				String value[] = values.split(":");
				Select loanOfficer = new Select(getElementByUsing(value[0]));
				loanOfficer.selectByVisibleText(value[1]);
				
			}
			
			/*
			 * //td[4]/input,//tr[,]/td[4]/input  */

			public void conditionsCheckbox(String values)
					throws Exception {
				System.out.println("************* click on checkbox ************");
				WebDriver driver = DriverFactory.getDriver();
				String[] codeParam = KWVariables.getVariables().get(values).split(",");
				int endValue = driver.findElements(By.xpath(codeParam[0])).size() - 1;
				System.out.println("--------------endValue------------"+endValue);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				Thread.sleep(3000);		
				for (int i = 3; i <= endValue; i++) {
						String val=codeParam[1]+i+codeParam[2];
						WebElement checkbox = driver.findElement(By.xpath(codeParam[1]+i+codeParam[2]));
						js.executeScript("window.scrollBy(4000,0)", checkbox);
						js.executeScript("arguments[0].scrollIntoView(true);", checkbox);
						checkbox.click();
				}
			}
			

	@Override
	public void checkPage() {
		// TODO Auto-generated method stub
	}
}
