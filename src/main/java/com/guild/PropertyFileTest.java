package com.guild;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFileTest {
	static String loansFilePath;
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		/*Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("loanscreatedfilepath.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println(prop.getProperty("filepath"));
			System.out.println(prop.getProperty("dbuser"));
			System.out.println(prop.getProperty("dbpassword"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}*/
		String pt = getLoansCreatedFileLocation("");
		
		File currentResDir = new File(pt + File.separator);
		System.out.println("currentResDir : "+currentResDir);
		
		currentResDir.mkdirs();
		String newLoansCreatedCSVFilePath = currentResDir.getAbsolutePath() + File.separator ;
		System.out.println("newLoansCreatedCSVFilePath :: "+newLoansCreatedCSVFilePath);

	}
	
	public static String getLoansCreatedFileLocation(String values){
		System.out.println("************* getLoansCreatedFileLocation **************");
		Properties prp = new Properties();
		InputStream ins = null;
		try {
			ins = new FileInputStream("loanscreatedfilepath.properties");
			prp.load(ins);
			loansFilePath=prp.getProperty("filepath");
			System.out.println("loansFilePath : "+loansFilePath);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (ins != null) {
				try {
					ins.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return loansFilePath;
		
	}

}
